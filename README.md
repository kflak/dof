# README

## Build

Depends on [openFrameworks](https://openframeworks.cc). Install this for
your platform first. :IMPORTANT: tested against v0.12. Older versions
will probably NOT work.

Next, make sure you have the following addons installed. Consult the
openFrameworks docs or, in the case of `ofxCv` and `ofxBlur`, go to the
website for instructions.

### External Dependencies

- [ofxBlur](https://github.com/kylemcdonald/ofxBlur)
- [ofxEasing](https://github.com/arturoc/ofxEasing)
- ofxGui
- ofxXmlSettings
- [ofxKaleidoscope](https://github.com/radamchin/ofxKaleidoscope)

```bash
cd ~/openframeworks/apps/myApps
git clone https://gitlab.com/kflak/dof.git
# or, if you use ssh (preferred)
git clone git@gitlab.com:kflak/dof.git
```

You will have to export the `$OF_ROOT` environment variable before
compiling:

```bash
export OF_ROOT="/absolute/path/to/your/of/root/folder"
```

Now you should be ready to run `make`:

```bash
cd dof
make -j8
make run
```

If on Linux you can run `make -j$(nproc)` instead to use all available
cores.

## Osc Commands

The application listens to OSC port 12345 by default. Currently this is
hard-coded. If you need to change this, you can find it in ofApp.h as
`m_port`. Change this to whatever you need and recompile.

In order to add functionality to the OSC responses, you need to add this
to the `handleOsc` method in `ofApp.cpp`. Should be reasonably simple to
implement. Just imitate whatever is there already.

`/scene <string>`

Change to one of the following scenes:

- "Black": blackout
- "SlitScan": Radial slitscan of the webcam image
- "CleanPhysarum": A physarum with no interaction
- "Tunnel": A slightly creepy video in b/w
- "CamFire": Webcam running through fire
- "BoxPhysarum": Physarum as a score for musicians
- "CamEdge": Edge detection with a dark color scheme
- "CamEdgeParticleSystem": Edge detection with a light color scheme,
- transforming into a particle system.
- "CamGlitch": Glitchy webcam
- "SunRain": Urban poetic rainy day.
- "CamTunnel": The tunnel from tunnel, but displaced by the webcam.
- "Spray": Happy bursts of particles
- "Caleidoscope": Processed input from the webcam

`dimmer <float>`

Set opacity of current scene. Range [0..1]

`slitscan/blurScale <float>`

Change the blur factor of the SlitScan scene. Reasonable range: [0..10]

`camEdgeParticleSystem/toggleParticles`

Toggle between edge detection and particle system.

`camEdgeParticleSystem/setAcceleration`

Set acceleration of the particles. Reasonable range: [-0.01..0.01]

`/mix <float, float>`

Generic parameter, means different things in different circumstances. In
Tunnel and CamTunnel it is in the range [-1.0..1.0] and notates the
amount of displacement. The second float is the smoothing factor.

`/spray/burst <numParticles, float x, float y, float z, float spread,
float lifespan, float acceleration, float hue>`

Parameters for creating a burst of particles in the `Spray` scene.

`hue` is in the range [0..255], `acceleration` in whatever you want, but
probably nice to stay in the [-1.0..1.0] range. `spread` is how far
apart the particles are in the beginning, in pixels. `x`, `y`, `z` are
the coordinates on the screen, beginning from x: 0, y:0 in the upper
left corner to x: 1920, y: 1080 in the lower right corner. z can be
anything you want. 0 is the middle of the coordinate system.

Example `SuperCollider` code:

```supercollider
n = NetAddr.new("localhost", 12345);
n.sendMsg("/scene", "Spray");
n.sendMsg("/spray/burst", rrand(100, 1000), rand(1920), rand(1080), rand(200), rand(1000), rrand(10, 20), rrand(-1.0, 1.0), rand(255));
```

`/setHeight, <int>`

For now only works in `Caleidoscope`. Sets the height of the
caleidoscope.

`/setSegments, <int>`

For Caleidoscope. Sets the number of segments. Multiples of 2, in the
range [4..]

`/setRotation, <float>`
For Caleidoscope. Sets the rotation speed.

Example `SuperCollider` code for a glitchy, dynamic resizing, rotation
and change of segments:

```supercollider
(
n = NetAddr.new("localhost", 12345);
f = fork{
    loop{
        var seg = rrand(4, 200);
        var height = rrand(10, 2000);
        var rotation = rrand(-0.2, 0.2);
        netAddr.sendMsg("/setHeight", height);
        netAddr.sendMsg("/setSegments", seg);
        netAddr.sendMsg("/setRotation", rotation);
        rrand(0.1, 1).wait;
    }
}
)
```

`/setSource <string>`

For now only implemented in Caleidoscope. Valid values:

- "webcam"
- "fireplace"
- "tunnel"
- "sunrain"

`/enableCameraDelta <bool>`

Enable or disable sending of delta values from the webcam. Only works
when scenes that use the webcam are active. The application sends the
normalized delta value [0..1] as a float to `/cameraDelta` on the
default port.

## Adding scenes

All the scenes are subclasses of the `Scene` superclass, and need to
implement its methods. Add them to the `m_scenes` vector:

```cpp
#include "scene/yourscene/YourScene.h"

m_scenes.emplace("YourScene", make_shared<YourScene>());
m_scenes["YourScene"]->setup();
```

## Required methods

You need at the very least an `update` and a `draw` method, as well as a
`setup` method if your class needs to allocate resources at startup.

## Adding methods to scene

In order to add custom methods to a scene that don't exist in the other
scenes you can add these in `Scene.h` as a `virtual` method. Make sure
you _also_ add a default, empty implementation in `Scene.cpp`, otherwise
you will have to add the method to _all_ the other scenes. You don't
want to do that...

Example:

```cpp
// in Scene.h
virtual void myNewMethod();

// in Scene.cpp
Scene::myNewMethod(){}
```

## Keybindings

Add/change keybindings by editing the `keyPressed` method in `ofApp.cpp`

- `1` Scene SlitScan
- `2` Scene CamFire
- `3` Scene CleanPhysarum
- `4` Scene Text
- `5` Scene BoxPhysarum
- `6` Scene CamEdge
- `7` Scene CamGlitch
- `8` Scene Tunnel
- `9` Scene CamEdgeParticleSystem
- `0` Scene CamTunnel
- `s` Scene Spray
- `c` Scene Caleidoscope
- `i` Scene Instructions
- `t` Scene Sticks
- `a` Toggle autoMix
- `KEY_UP` (in Caleidoscope) increase height
- `KEY_DOWN` (in Caleidoscope) decrease height
- `j` (in Caleidoscope) decrease number of segments
- `k` (in Caleidoscope) increase number of segments
- `o` Send dimmervalue to osc address `"/dimmerValue"`
- `.` Show framerate
- `,` Show gui
- `q` Exit application

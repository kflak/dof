#version 150

out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform float dimmer;
uniform float r;
uniform float g;
uniform float b;
uniform float radius;
uniform int height;
const int quality = 8;
const int directions = 16;
const float two_pi = 6.28318530718;

in vec2 texCoordVarying;

void main() {
    vec3 physarum = texture(tex0, texCoordVarying).xyz;
    vec4 lines = texture(tex1, texCoordVarying);

    // for( float d=0.0;d<two_pi;d+=two_pi/float(directions) )
    // {
    //     for( float i=1.0/float(quality);i<=1.0;i+=1.0/float(quality) )
    //     {
    //         lines += texture(tex1, texCoordVarying+vec2(cos(d),sin(d))*radius*i);
    //     }
    // }
    // lines /= float(quality)*float(directions)+1.0;

    vec3 invertedColor = 1 - physarum.rgb;
    float mix = lines.r; 

    vec3 masked = (1 - mix) * physarum + (invertedColor * mix);

    masked.r *= r;
    masked.g *= g;
    masked.b *= b;

    outputColor = vec4(masked + lines.rgb, 1.0) * dimmer;
}


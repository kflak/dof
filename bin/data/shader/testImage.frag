#version 330

out vec4 outputColor;
uniform sampler2DRect tex0;

in vec2 texCoordVarying;

void main(){

    outputColor = texture( tex0, texCoordVarying);

}


#version 120
#extension GL_ARB_texture_rectangle : enable
#extension GL_EXT_gpu_shader4 : enable

uniform float phase = 0.0;
uniform float distortAmount = 0.25;

void main() {
    vec3 v = gl_Vertex.xyz;

    float distort = distortAmount * sin(phase + 0.015 * v.y);

    v.x /= 1.0 + distort;
    v.y /= 1.0 + distort;
    v.z /= 1.0 + distort;

    vec4 posHomog = vec4(v, 1.0);
    gl_Position = gl_ModelViewPojectionMatrix * posHomog;

    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_FrontColor = gl_Color;
}

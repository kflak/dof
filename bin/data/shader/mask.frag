#version 330

in vec2 texCoordVarying;
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform float dimmer;
uniform float red = 1.0;
uniform float green = 1.0;
uniform float blue = 1.0;
uniform float multiply = 1.0;
uniform float add = 0.0;

void main() {
    vec3 cin0 = texture(tex0, texCoordVarying).xyz;
    vec3 cin1 = texture(tex1, texCoordVarying).xyz;
    
    cin1 += add;
    cin0 *= cin1;
    cin0 *= vec3(red, green, blue);
    cin0 *= multiply;

    outputColor = vec4(cin0, dimmer);
}

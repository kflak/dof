#version 330
in vec2 texCoordVarying;
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int w;
uniform int h;
uniform float red = 1.0;
uniform float green = 1.0;
uniform float blue = 1.0;
uniform float dimmer;
uniform float rotation = 1.3;
uniform int horizontalFlip = 0;
uniform int invertColor = 0;
// 2d rotation matrix
mat2 rotate(float angle) {
    float cos_a = cos(angle);
    float sin_a = sin(angle);
    return mat2(cos_a, -sin_a,
                sin_a,  cos_a);
}

void main() {
    vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x, h-gl_FragCoord.y)).xyz;

    vec2 rotated = rotate(0.5*cin0.x) 
        * vec2(
            abs((w * horizontalFlip)-gl_FragCoord.x), 
            h-gl_FragCoord.y
        );

    vec3 cin1 = texture(tex1, rotated * rotation).xyz;


    cin1.r *= red;
    cin1.g *= green;
    cin1.b *= blue;

    cin1 = abs(invertColor - cin1);

    outputColor = vec4(cin1, 1.0);
    // float red = r /= 255;
    // outputColor = vec4(cin1, 1.0) * dimmer;
}


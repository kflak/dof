#version 150

uniform sampler2DRect tex;
uniform float dR;
uniform int w; // width
uniform int h; // height
uniform float strength;
uniform float scale;
uniform float off;
uniform vec2 center;

in vec2 texCoordVarying;

out vec4 outputColor;

void main(){
	vec2 pos = texCoordVarying.xy;
	vec4 col;

	float decayRate = dR;

	//DIFFUSE
	col = texture(tex, pos);
	col += texture(tex, pos + vec2(-1,-1));
	col += texture(tex, pos + vec2(0,-1));
	col += texture(tex, pos + vec2(1,-1));
	col += texture(tex, pos + vec2(-1,0));
	col += texture(tex, pos + vec2(1,0));
	col += texture(tex, pos + vec2(-1,1));
	col += texture(tex, pos + vec2(0,1));
	col += texture(tex, pos + vec2(1,1));

	col /= 9.0;
	
	//DECAY
    // float dist_from_center = distance(vec2(pos.x/w, pos.y/h), center);
    // dist_from_center = clamp(off + scale * dist_from_center, 0.0, 1.0);
	// col.rgb *= decayRate * pow(dist_from_center, strength);
    col.rgb *= decayRate;

 	outputColor = col;
	// if(distance(vec2(pos.x/w, pos.y/h), center) < 0.01) {
        // outputColor = vec4(1);
    // }
}

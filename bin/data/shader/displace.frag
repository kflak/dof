#version 330
in vec2 texCoordVarying;
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
// uniform sampler2DRect tex2;
uniform int w;
uniform int h;
uniform float dimmer = 1.0;
uniform float mix;
uniform float invertColor = 0;

void main() {
    vec3 cin0 = texture(tex0, texCoordVarying).xyz;

    float mulR = 1 - ((1 - cin0.r) * mix);

    cin0 = texture(tex1, vec2(texCoordVarying.x * mulR, texCoordVarying.y * mulR)).xyz;

    cin0 = abs(invertColor - cin0);

    // cin0 = cin0 + (texture(tex2, texCoordVarying).xyz * 0.5);

    outputColor = vec4(cin0, dimmer);
}

#version 330
out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int h; // height
uniform float dimmer;

void main()
{
    vec3 cin0 = texture(tex0, vec2(gl_FragCoord.x, h-gl_FragCoord.y)).xyz;
    /* vec3 cin0 = texture(tex0, gl_FragCoord.xy).xyz; */
    outputColor = vec4(dimmer * cin0, 1.0);
}


#version 330

out vec4 outputColor;

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform int h;
uniform int w;
uniform float dimmer;

in vec2 texCoordVarying;

void main()
{
    // vec4 cin0 = texture(tex0, texCoordVarying);
    // vec4 cin1 = texture(tex1, texCoordVarying);

    // cin0.rgb = cin1.gbr;

    // outputColor = cin1;

    vec3 cin0 = texture(tex0, texCoordVarying).xyz;
    vec3 cin1 = texture(tex1, vec2(texCoordVarying.x, h-texCoordVarying)).xyz;

    // outputColor = texture(tex1, texCoordVarying);
    outputColor = vec4(0.7 * (cin0 + cin1), 1.0);
}


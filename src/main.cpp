#include "ofMain.h"
#include "ofApp.h"
#include "Configuration.h"


//========================================================================
int main( ){

	//Use ofGLFWWindowSettings for more options like multi-monitor fullscreen
	ofGLWindowSettings settings;
    settings.setGLVersion(3, 2);
    if(Configuration::screenMode == ScreenMode::WINDOW) {
//        settings.setPosition( vec2(Configuration::windowWidth/2, 0) );
        settings.setPosition( ofVec2f(0, 0) );
        settings.windowMode = OF_WINDOW;
    } else {
        settings.setPosition( ofVec2f(0, 0) );
        settings.windowMode = OF_FULLSCREEN;
    }
    settings.setSize(Configuration::windowWidth, Configuration::windowHeight);

	auto window = ofCreateWindow(settings);

	ofRunApp(window, make_shared<DoF>());
	ofRunMainLoop();

}

#include "ofApp.h"
#include <format>
#include "cameraDelta/cameraDelta.h"
#include "concentric/Concentric.h"
#include "easingTest/easingTest.h"
#include "globalvars.h"
#include "instructions.h"
#include "mathHelpers.h"
#include "ofTrueTypeFont.h"
#include "scene/black/Black.h"
#include "scene/caleidoscope/caleidoscope.h"
#include "scene/camedge/CamEdge.h"
#include "scene/camedge/CamEdgeParticleEmitter.h"
#include "scene/camedge/CamEdgeParticleSystem.h"
#include "scene/camfire/CamFire.h"
#include "scene/camglitch/CamGlitch.h"
#include "scene/camtunnel/CamTunnel.h"
#include "scene/circleDisplay/circleDisplay.h"
#include "scene/easingTest/easingTest.h"
#include "scene/movie/SunRain.h"
#include "scene/movie/Tunnel.h"
#include "scene/movie/river.h"
#include "scene/physarum/BoxPhysarum.h"
#include "scene/physarum/CleanPhysarum.h"
#include "scene/physarum/DonutPhysarum.h"
#include "scene/slitscan/SlitScan.h"
#include "scene/spray/Spray.h"
#include "scene/text/text.h"
#include "sphere/Sphere.h"
#include "sticks/sticks.h"

//--------------------------------------------------------------
void DoF::setup() {

    setting.load("settings.xml");

    ofSetVerticalSync(Configuration::verticalSync);

    setupWebcam();

    // set up movies
    movie.emplace("fireplace", make_shared<ofVideoPlayer>());
    movie["fireplace"]->load("movie/fireplace.mp4");
    movie["fireplace"]->setLoopState(OF_LOOP_NORMAL);

    movie.emplace("sunrain", make_shared<ofVideoPlayer>());
    movie["sunrain"]->load("movie/SunRain.mp4");
    movie["sunrain"]->setLoopState(OF_LOOP_NORMAL);

    movie.emplace("tunnel", make_shared<ofVideoPlayer>());
    movie["tunnel"]->load("movie/tunnel.mp4");
    movie["tunnel"]->setLoopState(OF_LOOP_NORMAL);

    movie.emplace("snow", make_shared<ofVideoPlayer>());
    movie["snow"]->load("movie/snowyBackground.mp4");
    movie["snow"]->setLoopState(OF_LOOP_NORMAL);

    movie.emplace("river", make_shared<ofVideoPlayer>());
    movie["river"]->load("movie/river.mp4");
    movie["river"]->setLoopState(OF_LOOP_NORMAL);

    // set up scenes

    m_scenes.emplace("Black", make_shared<Black>());

    m_scenes.emplace("SlitScan", make_shared<SlitScan>());

    m_scenes.emplace("CleanPhysarum", make_shared<CleanPhysarum>());
    // m_scenes["CleanPhysarum"]->setup();

    m_scenes.emplace("Tunnel", make_shared<Tunnel>());
    m_scenes["Tunnel"]->setup();

    m_scenes.emplace("CamTunnel", make_shared<CamTunnel>());
    m_scenes["CamTunnel"]->setup();

    m_scenes.emplace("CamFire", make_shared<CamFire>());
    m_scenes["CamFire"]->setup();
    m_scenes["CamFire"]->play();

    m_scenes.emplace("BoxPhysarum", make_shared<BoxPhysarum>());
    m_scenes["BoxPhysarum"]->setup();

    m_scenes.emplace("CamEdge", make_shared<CamEdge>());
    m_scenes["CamEdge"]->setup();

    m_scenes.emplace("CamEdgeParticleSystem",
                     make_shared<CamEdgeParticleSystem>());
    m_scenes["CamEdgeParticleSystem"]->setup();

    m_scenes.emplace("CamEdgeParticleEmitter",
                     make_shared<CamEdgeParticleEmitter>());
    m_scenes["CamEdgeParticleEmitter"]->setup();

    m_scenes.emplace("CamGlitch", make_shared<CamGlitch>());
    m_scenes["CamGlitch"]->setup();

    m_scenes.emplace("SunRain", make_shared<SunRain>());
    m_scenes["SunRain"]->setup();

    m_scenes.emplace("Text", make_shared<Text>());
    m_scenes["Text"]->setup();

    m_scenes.emplace("Spray", make_shared<Spray>());
    m_scenes["Spray"]->setup();

    m_scenes.emplace("EasingTest", make_shared<EasingTest>());
    m_scenes["EasingTest"]->setup();

    m_scenes.emplace("Caleidoscope", make_shared<Caleidoscope>());
    m_scenes["Caleidoscope"]->setup();

    m_scenes.emplace("CircleDisplay", make_shared<CircleDisplay>());
    m_scenes["CircleDisplay"]->setup();

    m_scenes.emplace("Instructions", make_shared<Instructions>());
    m_scenes["Instructions"]->setup();

    m_scenes.emplace("Sticks", make_shared<Sticks>());
    m_scenes["Sticks"]->setup();

    m_scenes.emplace("River", make_shared<River>());
    m_scenes["River"]->setup();

    m_scenes.emplace("DonutPhysarum", make_shared<DonutPhysarum>());
    m_scenes["DonutPhysarum"]->setup();

    m_scenes.emplace("Concentric", make_shared<Concentric>());
    m_scenes["Concentric"]->setup();

    m_scenes.emplace("Sphere", make_shared<Sphere>());
    m_scenes["Sphere"]->setup();

    m_current = m_scenes["Black"];
    switchScene("Black");

    ofSetFrameRate(Configuration::fps);

    m_receiver.setup(m_PORT);
    m_sender.setup(m_externalHost, m_externalPort);
    m_cameraDelta.setup();
}

//--------------------------------------------------------------
void DoF::update() {

    handleOsc();

    if (m_current) {
        m_current->update();
    }

    if (enableCameraDelta) {
        m_cameraDelta.update();
        ofxOscMessage msg;
        msg.setAddress("/cameraDelta");
        msg.addFloatArg(cameraDelta);
        m_sender.sendMessage(msg);
    }
}

//--------------------------------------------------------------
void DoF::draw() {

    ofSetBackgroundColor(0);

    if (m_current) {
        m_current->draw();
    }

    ofEnableAlphaBlending();
    int dim = 255 - (dimmer * 255);
    ofSetColor(0, 0, 0, dim);
    ofDrawRectangle(0, 0, w, h);
    ofDisableAlphaBlending();

    if (m_showFrameRate) {
        const int xOffset = 20;
        const int yOffset = 20;
        ofDrawBitmapString("fps: " + ofToString(ofGetFrameRate()), xOffset,
                           yOffset);

        ofDrawBitmapString("cameraDelta: " + ofToString(cameraDelta), xOffset,
                           yOffset * 2);
    }
}

//--------------------------------------------------------------
void DoF::keyPressed(int key) {
    // delegate key press only to the currently active scene.
    if (m_current) {
        m_current->keyPressed(key);
    }

    switch (key) {
        case '.':
            m_showFrameRate = !m_showFrameRate;
            break;
        case '-':
            break;
        case 'q': {
            for (auto &scene : m_scenes) {
                // do necessary cleanup, if necessary
                scene.second->exit();
            }
            ofxOscMessage msg;
            msg.setAddress("/s_quit");
            m_sender.sendMessage(msg);
            ofExit(0);
        } break;
        case ',':
            showGui = !showGui;
            break;
        case '1':
            switchScene("SlitScan");
            break;
        case '2':
            switchScene("CamFire");
            m_scenes["CamFire"]->play();
            break;
        case '3':
            switchScene("CleanPhysarum");
            break;
        case '4':
            switchScene("Text");
            break;
        case '5':
            switchScene("BoxPhysarum");
            m_scenes["BoxPhysarum"]->play();
            break;
        case '6':
            switchScene("CamEdge");
            m_scenes["CamEdge"]->play();
            break;
        case '7':
            switchScene("CamGlitch");
            break;
        case '8':
            switchScene("Tunnel");
            m_scenes["Tunnel"]->play();
            break;
        case '9':
            switchScene("CamEdgeParticleSystem");
            m_scenes["CamEdgeParticleSystem"]->play();
            break;
        case '0':
            switchScene("CamTunnel");
            m_scenes["CamTunnel"]->play();
            m_scenes["CamTunnel"]->resetTime();
            break;
        case 'b': {
            switchScene("Concentric");
            m_scenes["Concentric"]->setup();
            break;
        }
        case 's': {
            switchScene("Spray");
            size_t numParticles = ofRandom(1000);
            ofVec3f pos = {
                ofRandom(w),
                ofRandom(h),
                ofRandom(500),
            };
            float spread = ofRandom(1000);
            float lifespan = ofRandom(10, 20);
            float acceleration = ofRandom(-0.1, 0.1);
            float hue = ofRandom(255);
            m_scenes["Spray"]->burst(numParticles, pos, spread, lifespan,
                                     acceleration, hue);
            break;
        }
        case 'e': {
            switchScene("EasingTest");
            m_scenes["EasingTest"]->setup();
            break;
        }
        case 'c': {
            switchScene("Caleidoscope");
            m_scenes["Caleidoscope"]->play();
            break;
        }
        case 'd': {
            switchScene("CircleDisplay");
            break;
        }
        case 'i': {
            switchScene("Instructions");
            m_scenes["Instructions"]->play();
            break;
        }
        case 'r': {
            switchScene("River");
            m_scenes["River"]->play();
            break;
        }
        case 't': {
            switchScene("Sticks");
            break;
        }
        case 'p': {
            switchScene("DonutPhysarum");
            break;
        }
        case 'a':
            autoMix = !autoMix;
            break;
        case 'o': {
            ofxOscMessage msg;
            msg.setAddress("/dimmerValue");
            msg.addFloatArg(dimmer);
            m_sender.sendMessage(msg);
            break;
        }
        case 'g': {
            switchScene("Sphere");
            m_scenes["Sphere"]->setup();
            break;
        }
        default:
            break;
    }
}

void DoF::switchScene(const string &nextScene) {
    m_current = m_scenes[nextScene];
    ofLogNotice() << "Switched to scene: " << nextScene;
    for (const auto &scene : m_scenes) {
        const string sceneName = scene.first;
        if (sceneName != nextScene) {
            m_scenes[sceneName]->pause();
        } else {
            m_scenes[sceneName]->play();
        }
    }
}

void DoF::handleOsc() {

    while (m_receiver.hasWaitingMessages()) {
        ofxOscMessage message;
        m_receiver.getNextMessage(message);

        const string address = message.getAddress();

        switch (hash_djb2a(address)) {
            case "/scene"_sh: {
                const string scene = message.getArgAsString(0);
                switchScene(scene);
                m_scenes[scene]->play();
                break;
            }
            case "/dimmer"_sh:
                dimmer = message.getArgAsFloat(0);
                break;
            case "/slitscan/blurScale"_sh:
                m_scenes["SlitScan"]->blurScale = message.getArgAsFloat(0);
                break;
            case "/enableCameraDelta"_sh:
                enableCameraDelta = message.getArgAsBool(0);
                break;
            case "/camEdgeParticleSystem/cameraDeltaThreshold"_sh:
                m_scenes["CamEdgeParticleSystem"]->setCameraDeltaThreshold(
                    message.getArgAsFloat(0));
                break;
            case "/camEdgeParticleSystem/toggleParticles"_sh:
                m_scenes["CamEdgeParticleSystem"]->toggleState();
                break;
            case "/camEdgeParticleSystem/setAcceleration"_sh: {
                const float accX = message.getArgAsFloat(0);
                const float accY = message.getArgAsFloat(1);
                const ofVec2f acc = {accX, accY};
                m_scenes["CamEdgeParticleSystem"]->setAcceleration(acc);
                break;
            }
            case "/spray/burst"_sh: {
                const int numParticles = message.getArgAsInt(0);
                const float x = message.getArgAsFloat(1);
                const float y = message.getArgAsFloat(2);
                const float z = message.getArgAsFloat(3);
                const float spread = message.getArgAsFloat(4);
                const float lifespan = message.getArgAsFloat(5);
                const float acceleration = message.getArgAsFloat(6);
                const float hue = message.getArgAsFloat(7);
                m_scenes["Spray"]->burst(numParticles, ofVec3f(x, y, z), spread,
                                         lifespan, acceleration, hue);
                break;
            }
            case "/toggleState"_sh: {
                m_current->toggleState();
                break;
            }
            case "/invertColor"_sh: {
                m_current->invertColor();
                break;
            }
            case "/mix"_sh: {
                const float mix = message.getArgAsFloat(0);
                m_current->setMix(mix);
                break;
            }
            case "/resetTime"_sh: {
                m_current->resetTime();
                break;
            }
            case "/getDimmer"_sh: {
                ofxOscMessage msg;
                msg.setAddress("/dimmerValue");
                msg.addFloatArg(dimmer);
                m_sender.sendMessage(msg);
                break;
            }
            case "/setHost"_sh: {
                m_externalHost = message.getArgAsString(0);
                break;
            }
            case "/setHeight"_sh: {
                const size_t height = message.getArgAsInt(0);
                m_current->setHeight(height);
                break;
            }
            case "/setSegments"_sh: {
                const size_t seg = message.getArgAsInt(0);
                m_current->setSegments(seg);
                break;
            }
            case "/setRotation"_sh: {
                const float rot = message.getArgAsFloat(0);
                m_current->setRotation(rot);
                break;
            }
            case "/setSource"_sh: {
                const string &source = message.getArgAsString(0);
                m_current->setSource(source);
                break;
            }
            case "/setRotationSpeed"_sh: {
                const float speed = message.getArgAsFloat(0);
                m_current->setRotationSpeed(speed);
                break;
            }
            case "/createSticks"_sh: {
                const size_t numSticks = message.getArgAsInt(0);
                m_current->createSticks(numSticks);
                break;
            }
            case "/setProbability"_sh: {
                const float prob = message.getArgAsFloat(0);
                m_current->setProbability(prob);
                break;
            }
            case "/depositAmount"_sh: {
                const float depositAmount = message.getArgAsFloat(0);
                m_current->setDepositAmount(depositAmount);
                break;
            }
            case "/setSenseDistance"_sh: {
                const float senseDist = message.getArgAsFloat(0);
                m_current->setSenseDistance(senseDist);
            }
            case "/maxAngle"_sh: {
                const float maxAngle = message.getArgAsFloat(0);
                m_current->setMaxAngle(maxAngle);
                break;
            }
            case "/numDirections"_sh: {
                const int numDirections = message.getArgAsInt(0);
                m_current->setNumDirections(numDirections);
            }
            case "/setMaxSpeed"_sh: {
                const float maxSpeed = message.getArgAsFloat(0);
                m_current->setMaxSpeed(maxSpeed);
                break;
            }
            case "/setParticleMode"_sh: {
                const int mode = message.getArgAsInt(0);
                m_current->setParticleMode(mode);
                ofLogNotice() << mode;
                break;
            }
            case "/setHSV"_sh: {
                const float hue = message.getArgAsFloat(0);
                const float sat = message.getArgAsFloat(1);
                const float val = message.getArgAsFloat(2);
                m_current->setHSV(hue, sat, val);
                break;
            }
            case "/setDecayRate"_sh: {
                const float rate = message.getArgAsFloat(0);
                m_current->setDecayRate(rate);
                break;
            }
            case "/setTrailHistory"_sh: {
                const float trailHistory = message.getArgAsFloat(0);
                m_current->setTrailHistory(trailHistory);
                break;
            }
            case "/setWrap"_sh: {
                const int wrap = message.getArgAsInt(0);
                m_current->setWrap(wrap);
                break;
            }
            default: {
                ofLogWarning() << "OSC command not assigned";
            }
        }
    }
}

void DoF::setupWebcam() {

    // get a list of devices
    vector<ofVideoDevice> devices = webcam.listDevices();

    vector<string> names;

    for (auto &device : devices) {
        if (device.bAvailable) {
            //log the device
            ofLogNotice() << device.id << ": " << device.deviceName;
            names.push_back(device.deviceName);
        } else {
            ofLogNotice() << device.id << ": " << device.deviceName
                          << " - unavailable ";
        }
    }

    if (std::find(names.begin(), names.end(), "Rapoo Camera: Rapoo Camera") !=
        names.end()) {
        camWidth = w;
        camHeight = h;
    }

    webcam.setDeviceID(0);
    webcam.setDesiredFrameRate(Configuration::fps);
    webcam.initGrabber(camWidth, camHeight);
}

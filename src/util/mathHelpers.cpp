#include "mathHelpers.h"
#include <cmath>

ofVec2f polarToCartesian(ofVec2f polar) {
    const float radius = polar[0];
    const float angle = polar[1];
    const float x = radius * std::cos(angle);
    const float y = radius * std::sin(angle);
    return {x, y};
}

ofVec2f cartesianToPolar(ofVec2f cartesian) {
    const float x = cartesian.x;
    const float y = cartesian.y;
    const float radius = std::sqrt(x * x + y * y);
    const float angle = std::atan2(y, x);
    return {radius, angle};
}

#pragma once
#include <string>
#include <vector>

#include "operate.h"
#include "Configuration.h"

#define STRINGIFY(A) #A

extern void split(std::string const &blob, const char separator, std::vector<std::string> &parts);
extern std::string trim(std::string const &str, const char start = ' ', const char end = ' ');
extern std::string trim(std::string const &str, std::string const &start, std::string const & end);
extern std::string cleanWhite(std::string const &str);
extern void findAndReplaceAll(std::string &data, std::string toSearch, std::string replaceStr);
extern bool hasEnding(std::string const &fullString, std::string const &ending);

#ifdef WITH_testcases
extern void stringhelper_runTestCases();
#endif // WITH_testcases


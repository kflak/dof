#pragma once
#include "ofVec2f.h"

ofVec2f polarToCartesian(ofVec2f polar);
ofVec2f cartesianToPolar(ofVec2f cartesian);

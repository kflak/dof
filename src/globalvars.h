#pragma once

#include "ofMain.h"

#include "Scene.h"
#include "ofxXmlSettings.h"

extern std::map<string, shared_ptr<ofVideoPlayer>> movie;

extern unsigned int frame;
extern float dimmer;

extern ofxXmlSettings setting;

extern bool showGui;

extern const int w;
extern const int h;

extern const int centerW;
extern const int centerH;

extern int camWidth;
extern int camHeight;
extern ofVideoGrabber webcam;

extern bool autoMix;

extern float cameraDelta;
extern bool enableCameraDelta;

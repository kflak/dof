#pragma once

#include "Configuration.h"
#include "globalvars.h"
#include "ofMain.h"
#include "ofxXmlSettings.h"

/// \class base class for all scene classes
/// \brief provides empty default functions for derivative subclasses

class Scene {
   public:
    Scene();
    virtual ~Scene();

    /// Set up scene class
    /// Must be called only once.
    ///
    /// Call setup after openFrameworks is fully initialized.
    /// This is for reservation of system resources like network connections.
    virtual void setup();

    virtual void exit();

    /// Per frame update of the data model.
    /// Overwrite this function only if a subclass requires its own specific algorithm.
    virtual void update();

    /// Per frame drawing.
    /// Every subclass should provide its own implementation.
    virtual void draw();

    /// Play function. Use this to set the play state of videos
    virtual void play();
    virtual void pause();

    virtual void keyPressed(int key);

    const float defaultBlurScale = 3.0;
    float blurScale = defaultBlurScale;

    virtual void toggleState();
    virtual void setAcceleration(ofVec2f acc);
    virtual void invertColor();
    virtual void setMix(float mix);
    virtual void resetTime();
    virtual void setLag(float lag);
    virtual void burst(size_t numParticles, ofVec3f center, float spread,
                       float lifespan, float acceleration, float hue);
    virtual void setHeight(size_t height);
    virtual void setRotation(float rotation);
    virtual void setSegments(size_t segments);
    virtual void setSource(const string &source);
    virtual void setRotationSpeed(float rotationSpeed);
    virtual void createSticks(size_t numSticks);
    virtual void randomize();
    virtual void setProbability(float probability);
    virtual void setDepositAmount(float amount);
    virtual void setMaxAngle(float angle);
    virtual void setNumDirections(int numDirs);
    virtual void setMaxSpeed(float maxSpeed);
    virtual void setParticleMode(size_t particleMode);
    virtual void setSenseDistance(float senseDist);
    virtual void setDecayRate(float rate);
    virtual void setWrap(int wrap);
    virtual void setHSV(float hue, float saturation, float value);
    virtual void setCameraDeltaThreshold(float threshold);
    bool isMovie(const string &source);
    virtual void setTrailHistory(float trailHistory);

   protected:
};

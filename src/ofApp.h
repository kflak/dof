#pragma once

#include <algorithm>
#include <iterator>
#include "Configuration.h"
#include "Scene.h"
#include "cameraDelta/cameraDelta.h"
#include "ofGraphicsBaseTypes.h"
#include "ofImage.h"
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxXmlSettings.h"
#include "score/ScoreLine.h"
#include "util/stringhasher.h"

class DoF : public ofBaseApp {
   public:
    void setup() override;
    void update() override;
    void draw() override;

    void keyPressed(int key) override;

    /// Selects which scene is visible.
    void switchScene(const string &nextScene);
    void handleOsc();

   private:
    /// pointer to the visible scene
    shared_ptr<Scene> m_current;

    /// all scenes that are set up. But only one is active and visible.
    map<string, shared_ptr<Scene>> m_scenes;

    bool m_showFrameRate = false;

    const int m_PORT = 12345;
    ofxOscReceiver m_receiver;

    ofxOscSender m_sender;
    string m_externalHost = "127.0.0.1";
    const int m_externalPort = 57120;

    ofImage m_testImage;
    ofShader m_shader;
    static void setupWebcam();
    CameraDelta m_cameraDelta;
};

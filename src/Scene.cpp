#include "Scene.h"

Scene::Scene() = default;
Scene::~Scene() = default;

void Scene::setup() {}

void Scene::exit() {}
void Scene::update() {}
void Scene::draw() {}
void Scene::play() {}
void Scene::pause() {}
void Scene::toggleState() {}
void Scene::setAcceleration(const ofVec2f acc) {}
void Scene::invertColor() {}
void Scene::setMix(float mix) {}
void Scene::resetTime() {};
void Scene::setLag(float lag) {};
void Scene::burst(size_t numParticles, ofVec3f center, float spread,
                  float lifespan, float acceleration, float hue) {};
void Scene::setHeight(size_t height) {};
void Scene::setRotation(float rotation) {};
void Scene::setSegments(size_t segments) {};
void Scene::setSource(const string &source) {};
void Scene::keyPressed(int key) {}
void Scene::setRotationSpeed(float rotationSpeed) {};
void Scene::randomize() {};
void Scene::createSticks(size_t numSticks) {};
bool Scene::isMovie(const string &source) {
    auto iter = movie.find(source);
    return iter != movie.end();
}
void Scene::setProbability(float probability) {};
void Scene::setDepositAmount(float amount) {};
void Scene::setMaxAngle(float angle) {};
void Scene::setNumDirections(int numDirs) {};
void Scene::setMaxSpeed(float maxSpeed) {};
void Scene::setParticleMode(size_t particleMode) {};
void Scene::setSenseDistance(float senseDist) {};
void Scene::setDecayRate(float rate) {};
void Scene::setWrap(int wrap) {};
void Scene::setHSV(float hue, float saturation, float value) {};
void Scene::setCameraDeltaThreshold(float threshold) {}
void Scene::setTrailHistory(float trailHistory) {}

#pragma once
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <cstddef>

enum class ScreenMode {
	FULLSCREEN, WINDOW,
};

//TODO should be read from a configuration file.

/// \class compile time configuration.
/// \brief provides empty default functions for derivative subclasses
class Configuration {
public:
	/// two windows, half size, for debugging
//	static const ScreenMode screenMode = ScreenMode::WINDOW;
	/// two full screen areas without window decoration
	static const ScreenMode screenMode = ScreenMode::FULLSCREEN;

	static constexpr auto windowWidth  = (screenMode == ScreenMode::FULLSCREEN) ? 1920 : 960;
	static constexpr auto windowHeight = (screenMode == ScreenMode::FULLSCREEN) ? 1080 : 540;

	/// desired frame rate
	static const int fps = 30;
	// static const int fps = 60;

	/// openFrameworks syncs image flipping per window.
	/// 60 fps can lead to 30 fps in a 2 window application if openFrameworks sets verticalSync = true
	/// and the NVIDIA driver has its own strategy.
	static const bool verticalSync = false;

	/// trace time in nanoseconds, for debugging only.
	static const bool time_trace = false;

};

#pragma once

#include "Configuration.h"
#include "Scene.h"
#include "ofMain.h"
#include "ofxBlur.h"
#include "ofxOpenCv.h"

class TextFace : public Scene {

   public:

    void setup() override;
    void update() override;
    void draw() override;

    void keyPressed(int key) override;
    void learnBackground();

   private:
    float m_defaultHistory = 0.99;

   public:
    float trailHistory = m_defaultHistory;
    float alpha;

   private:
    ofPixels m_pixels;
    ofxCvColorImage m_image;
    ofxCvGrayscaleImage	m_grayImage;
    ofxCvGrayscaleImage	m_grayBg;
    ofxCvGrayscaleImage	m_grayDiff;
    ofxCvGrayscaleImage	m_grayDiffScaled;
    ofxCvContourFinder m_contourFinder;

    float m_threshold = initThreshold;
    bool m_learnBackground = true;
    int m_camwidth;
    int m_camheight;

    ofFbo m_fbo;
    ofxBlur m_blur;
    int m_blurRadius = 10;
    float m_blurShape = 0.2;
    int m_blurNumPasses = 5;
    float m_maxBlurScale = 10.0;

    static const int initThreshold = 100;
};

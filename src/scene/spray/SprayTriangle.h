#pragma once
#include "ofMain.h"

class SprayTriangle {
   public:
    SprayTriangle(ofVec3f pos, float hue, float speed, float lifespan);
    void update();
    void draw();

    void setAcceleration(ofVec3f acc) { m_acc = acc; };
    // void setLength(float length) { m_sideLength = length; }
    void setColor(ofColor color) { m_color = color; }
    // void setLifespan(float lifespan) { m_lifespan = lifespan; }

    inline auto getPos() const { return m_pos; };
    inline auto getAcc() const { return m_acc; };
    inline auto isAlive() const { return m_isAlive; };

   private:
    ofVec3f m_pos;
    // const ofColor m_defaultColor = ofColor::fromHsb(255, 255, 255);
    ofColor m_color;
    ofVec3f m_acc;
    float m_maxSpeed;
    ofVec3f m_speed = {
        ofRandom(-m_maxSpeed, m_maxSpeed),
        ofRandom(-m_maxSpeed, m_maxSpeed),
        ofRandom(-m_maxSpeed, m_maxSpeed),
    };
    // const float m_defaultRadius = 10;
    float m_sideLength1;
    float m_sideLength2;
    bool m_isAlive;
    float m_lifespan;
    float m_creationTime;
    float m_age = 0;
    float m_hue;
    float m_angle;
    float m_angleInc;
    ofVec3f m_vertex1;
    ofVec3f m_vertex2;
    ofVec3f m_vertex3;
};

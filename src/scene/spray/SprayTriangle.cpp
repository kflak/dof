#include "scene/spray/SprayTriangle.h"
#include "globalvars.h"
#include "ofxEasing.h"

SprayTriangle::SprayTriangle(ofVec3f pos, float hue, float speed,
                             float lifespan)
    : m_pos(pos),
      m_maxSpeed(speed),
      m_sideLength1(ofRandom(10, 1000)),
      m_sideLength2(ofRandom(10, 50)),
      m_isAlive(true),
      m_lifespan(lifespan),
      m_creationTime(ofGetElapsedTimef()),
      m_hue(hue),
      m_angle(ofRandom(360.0)),
      m_angleInc(ofRandom(-1.0, 1.0)) {
    m_color = ofColor::fromHsb(m_hue, 255, 255, 255);
}

void SprayTriangle::update() {
    auto now = ofGetElapsedTimef();
    m_age = now - m_creationTime;
    if (m_age > m_lifespan) {
        m_isAlive = false;
    }
    m_speed += m_acc;
    m_pos += m_speed;
    m_vertex1 = ofVec3f(0, 0, 0);
    m_vertex2 = m_vertex1 + ofVec3f(ofRandom(2), ofRandom(2), m_sideLength1);
    m_vertex3 = m_vertex1 + ofVec3f(ofRandom(2), m_sideLength2, ofRandom(2));
    m_color.setHue(m_hue);

    auto endTime = m_creationTime + m_lifespan;
    const float attackAsRatio = 16;
    auto attack = m_lifespan / attackAsRatio;
    auto attackFinishTime = m_creationTime + attack;

    m_color.a =
        (now < attackFinishTime)
            ? ofxeasing::map_clamp(now, m_creationTime, attackFinishTime, 0,
                                   255, &ofxeasing::linear::easeOut)
            : ofxeasing::map_clamp(now, attackFinishTime, endTime, 255, 0,
                                   &ofxeasing::cubic::easeIn);

    m_angle += m_angleInc;
}

void SprayTriangle::draw() {
    ofEnableAlphaBlending();
    ofSetColor(m_color);
    ofPushMatrix();
    ofTranslate(centerW, centerH);
    ofTranslate(m_pos);
    ofRotateZDeg(m_angle);
    ofDrawTriangle(m_vertex1, m_vertex2, m_vertex3);
    ofPopMatrix();
    ofDisableAlphaBlending();
}

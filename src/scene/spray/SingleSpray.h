#pragma once
#include "globalvars.h"
#include "ofMain.h"
#include "spray/SprayParticle.h"
#include "spray/SprayTriangle.h"

class SingleSpray {
   public:
    SingleSpray(size_t numParticles, ofVec3f center, float spread,
               float lifespan, float acc, float hue);
    SingleSpray();
    void update();
    void draw();
    bool isAlive() const{ return m_isAlive; }
    void setProbability(float probability) { m_probability = probability; }

   private:
    vector<SprayParticle> m_particles;
    vector<SprayTriangle> m_triangles;
    size_t m_numParticles;
    float m_startTime;
    ofVec3f m_center;
    float m_spread;
    float m_lifespan;
    bool m_isAlive;
    float m_acc;
    float m_hue;
    float m_probability;

    static constexpr size_t defaultNumParticles = 1000;
    static constexpr float defaultSpeed = 10;
    static constexpr float defaultSpread = 20;
    static constexpr float defaultLifespan = 5;
    static constexpr float defaultAcceleration = 0.1;
    static constexpr int defaultX = 1920/2;
    static constexpr int defaultY = 1080/2;
    static constexpr int defaultZ = 200;
    static constexpr float defaultHue = 255;
    static constexpr float defaultProbability = 0.8;
};

#include "scene/spray/SprayParticle.h"
#include "globalvars.h"
#include "ofxEasing.h"

SprayParticle::SprayParticle(ofVec3f pos, float hue, float speed,
                             float lifespan)
    : m_pos(pos),
      m_maxSpeed(speed),
      m_radius(ofRandom(3, 30)),
      m_isAlive(true),
      m_lifespan(lifespan),
      m_creationTime(ofGetElapsedTimef()),
      m_hue(hue) {
    m_color = ofColor::fromHsb(m_hue, 255, 255, 255);
}

void SprayParticle::update() {
    auto now = ofGetElapsedTimef();
    m_age = now - m_creationTime;
    if (m_age > m_lifespan) {
        m_isAlive = false;
    }
    m_speed += m_acc;
    m_pos += m_speed;
    m_color.setHue(m_hue);

    auto endTime = m_creationTime + m_lifespan;
    const float attackAsRatio = 16;
    auto attack = m_lifespan / attackAsRatio;
    auto attackFinishTime = m_creationTime + attack;

    m_color.a =
        (now < attackFinishTime)
            ? ofxeasing::map_clamp(now, m_creationTime, attackFinishTime, 0,
                                   255, &ofxeasing::linear::easeOut)
            : ofxeasing::map_clamp(now, attackFinishTime, endTime, 255, 0,
                                   &ofxeasing::cubic::easeIn);
}

void SprayParticle::draw() {
    ofPushMatrix();
    ofTranslate(centerW, centerH);
    ofEnableAlphaBlending();
    ofSetColor(m_color);
    ofDrawCircle(m_pos, m_radius);
    ofDisableAlphaBlending();
    ofPopMatrix();
}

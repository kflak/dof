#include "scene/spray/SingleSpray.h"
#include "globalvars.h"
SingleSpray::SingleSpray(size_t numParticles, ofVec3f center, float spread,
                         float lifespan, float acc, float hue)
    : m_numParticles(numParticles),
      m_startTime(ofGetElapsedTimef()),
      m_center(center),
      m_spread(spread),
      m_lifespan(lifespan),
      m_acc(acc),
      m_hue(hue) {
    m_probability = defaultProbability;
    auto probability = ofRandom(1) < m_probability;
    for (size_t i = 0; i < m_numParticles; i++) {
        // auto distance = ofRandom(m_spread);
        // auto angle = ofRandom(2*PI);
        // auto xOffset = cos(angle) * distance;
        // auto yOffset = sin(angle) * distance;
        // auto zOffset = ofRandom(-m_spread, m_spread);
        // auto xpos = m_center.x + xOffset;
        // auto ypos = m_center.y + yOffset;
        // auto zpos = m_center.z + zOffset;
        const auto minSat = 30;
        auto sat = ofRandom(minSat, 255);
        const auto minBright = 30;
        auto brightness = ofRandom(minBright, 255);
        auto color = ofColor::fromHsb(m_hue, sat, brightness);
        auto speed = ofRandom(defaultSpeed);
        const auto minLifespan = m_lifespan * 0.05;
        const auto maxLifespan = m_lifespan;
        auto current_lifespan = ofRandom(minLifespan, maxLifespan);
        if (probability) {
            SprayTriangle triangle = SprayTriangle({0, 0, 0}, m_hue,
                                                   speed, current_lifespan);
            triangle.setColor(color);
            triangle.setAcceleration(ofVec3f(ofRandom(-m_acc, m_acc),
                                             ofRandom(-m_acc, m_acc),
                                             ofRandom(-m_acc, m_acc)));
            // triangle.setLifespan(current_lifespan);
            m_triangles.push_back(triangle);
        } else {
            SprayParticle particle = SprayParticle({0, 0, 0}, m_hue,
                                                   speed, current_lifespan);
            particle.setColor(color);
            particle.setAcceleration(ofVec3f(ofRandom(-m_acc, m_acc),
                                             ofRandom(-m_acc, m_acc),
                                             ofRandom(-m_acc, m_acc)));
            // particle.setLifespan(current_lifespan);
            m_particles.push_back(particle);
        }
    }
}

SingleSpray::SingleSpray()
    : SingleSpray(defaultNumParticles, ofVec3f(defaultX, defaultY, defaultZ),
                  defaultSpread, defaultLifespan, defaultAcceleration,
                  defaultHue) {}

void SingleSpray::update() {
    auto time = ofGetElapsedTimef() - m_startTime;
    if (time < m_lifespan) {
        for (auto &particle : m_particles) {
            if (particle.isAlive()) {
                particle.update();
            }
        }
        for (auto &triangle : m_triangles) {
            if (triangle.isAlive()) {
                triangle.update();
            }
        }
    } else {
        m_isAlive = false;
        m_particles.clear();
        m_triangles.clear();
    }
}

void SingleSpray::draw() {
    if (m_isAlive) {
        for (auto &particle : m_particles) {
            if (particle.isAlive()) {
                particle.draw();
            }
        }
        for (auto &triangle : m_triangles) {
            if (triangle.isAlive()) {
                triangle.draw();
            }
        }
    }
}

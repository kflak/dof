#include "scene/spray/Spray.h"

void Spray::setup() {
    m_fbo.allocate(w, h);
    m_image.load("image/vignette1.png");
    m_shader.load("shader/mask");
}

void Spray::update() {
    for (size_t i = 0; i < m_sprays.size(); i++) {
        if (m_sprays[i].isAlive()) {
            m_sprays[i].update();
        } else {
            m_sprays.erase(m_sprays.begin() + i);
        }
    }

    m_fbo.begin();

    ofClear(0);
    const float gray = 0x333333;
    ofBackgroundGradient(ofColor::white, ofColor::fromHex(gray),
                         OF_GRADIENT_CIRCULAR);
    for (auto &spray : m_sprays) {
        spray.draw();
    }

    m_fbo.end();
}

void Spray::draw() {
    m_shader.begin();
    m_shader.setUniformTexture("tex0", m_fbo.getTexture(), 1);
    m_shader.setUniformTexture("tex1", m_image.getTexture(), 2);
    m_shader.setUniform1f("dimmer", dimmer);
    m_image.draw(0, 0);
    m_fbo.draw(0, 0);
    m_shader.end();
}

void Spray::burst(size_t numParticles, ofVec3f center, float spread,
                  float lifespan, float acceleration, float hue) {
    SingleSpray spray =
        SingleSpray(numParticles, center, spread, lifespan, acceleration, hue);
    m_sprays.push_back(spray);
}

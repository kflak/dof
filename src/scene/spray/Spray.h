#pragma once
#include "Scene.h"
#include "SingleSpray.h"
#include "ofMain.h"

class Spray : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void burst(size_t numParticles, ofVec3f center, float spread,
               float lifespan, float acceleration, float hue) override;
    void setProbability(float probability) override {
        for (auto &spray : m_sprays) {
            spray.setProbability(probability);
        }
    };

   private:
    ofFbo m_fbo;
    ofImage m_image;
    ofShader m_shader;
    vector<SingleSpray> m_sprays;
};

#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "SunRain.h"

void SunRain::setup() {

    m_shader.load("shader/glitch");
    m_fireplaceimg.load("movie/fireplace.png");
}

void SunRain::update() {
    movie["fireplace"]->update();
    movie["sunrain"]->update();
}


void SunRain::draw() {

    m_shader.begin();
    m_shader.setUniform1i("h", h);
    m_shader.setUniform1i("w", w);
    m_shader.setUniform1f("dimmer", dimmer);
    if(movie["fireplace"]->getTexture().isAllocated()){
        m_shader.setUniformTexture("tex0", movie["fireplace"]->getTexture(), 1);
    } else {
        m_shader.setUniformTexture("tex0", m_fireplaceimg.getTexture(), 1);
    }
    m_shader.setUniformTexture("tex1", movie["sunrain"]->getTexture(), 2);
    movie["sunrain"]->draw(0, 0);
    movie["fireplace"]->draw(0, 0);
    m_shader.end();
}

void SunRain::keyPressed(int key) {
}

void SunRain::play() {
    movie["fireplace"]->play();
    movie["sunrain"]->play();
}

void SunRain::pause() {
    movie["fireplace"]->setPaused(/*bPause=*/true);
    movie["sunrain"]->setPaused(/*bPause=*/true);
}

void SunRain::toggleState(){}

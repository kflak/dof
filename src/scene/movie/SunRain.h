#pragma once

#include "ofMain.h"
#include "Configuration.h"
#include "Scene.h"

class SunRain: public Scene {
public:

    void setup() override;
    void update() override;
    void draw() override;
    void keyPressed(int key) override;
    void play() override;
    void pause() override;
    void toggleState() override;

private:
    ofShader m_shader;
    ofImage m_fireplaceimg;

};


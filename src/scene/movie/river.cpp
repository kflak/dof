#include "river.h"
#include "Configuration.h"
#include "ofApp.h"
#include "ofMain.h"
#include "ofUtils.h"

void River::setup() {
    m_shader.load("shader/displace");
    m_fbo.allocate(w, h);
    ofBuffer names = ofBufferFromFile("riverText.txt");
    m_names = ofSplitString(names.getText(), "\n");
    m_sound.load(
        "/home/kf/Documents/REAPER "
        "Media/writingOurNamesInWater/writingOurNamesInWater1.wav");
    m_sound.setLoop(true);
}

void River::update() {
    movie["river"]->update();
    float t = ofGetElapsedTimef();
    if (t - m_time > m_interval) {
        RiverText r;
        r.setup();
        // r.setText(m_names[ofRandom(m_names.size() - 1)]);
        r.setText(m_names[m_nameIdx]);
        m_text.push_front(r);
        if (m_text.size() > m_max_names) {
            m_text.pop_back();
        };
        m_time = t;
        m_nameIdx++;
        m_nameIdx %= m_names.size();
    };

    m_fbo.begin();
    ofClear(0);
    for (auto &t : m_text) {
        t.update();
        t.draw();
    };
    // m_text.draw();
    m_fbo.end();
}

void River::draw() {

    // ofSetColor(0);
    m_shader.begin();

    m_shader.setUniform1i("h", h);
    m_shader.setUniform1i("w", w);
    m_shader.setUniform1f("dimmer", dimmer);

    if (m_autoMix) {
        const float timeScale = 0.016;
        const float minMix = 0.01;
        const float maxMix = 0.2;
        m_shaderMix = ofMap(sin(ofGetElapsedTimef() * timeScale), -1.0, 1.0,
                            minMix, maxMix);
    }
    m_shader.setUniform1f("mix", m_shaderMix);

    m_shader.setUniformTexture("tex0", m_fbo.getTexture(), 1);
    m_shader.setUniformTexture("tex1", movie["river"]->getTexture(), 2);

    movie["river"]->draw(0, 0);
    m_shader.end();
    ofEnableAlphaBlending();
    ofSetColor(200, 255, 255, 10);
    for (auto &t : m_text) {
        t.draw();
    };
    if (showGui) {
        ofSetColor(255);
        const float xOffset = 20;
        const float yOffset = 20;
        ofDrawBitmapString(ofToString(m_shaderMix), xOffset, yOffset);
    }
}

void River::keyPressed(int key) {
    const float inc = 0.01;
    switch (key) {
        case 'j': {
            m_shaderMix -= inc;
            m_shaderMix = ofClamp(m_shaderMix, -1.0, 1.0);
            break;
        }
        case 'k': {
            m_shaderMix += inc;
            m_shaderMix = ofClamp(m_shaderMix, -1.0, 1.0);
            break;
        }
        default:
            break;
    }
}

void River::play() {
    movie["river"]->play();
    m_sound.play();
}

void River::pause() {
    movie["river"]->setPaused(/*bPause=*/true);
    m_sound.setPaused(true);
}

void River::toggleState() {}

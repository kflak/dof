#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "Tunnel.h"

void Tunnel::setup() {
    m_shader.load("shader/displace");
}

void Tunnel::update() {
    movie["tunnel"]->update();
}



void Tunnel::draw() {

    // ofSetColor(0);
    m_shader.begin();

    m_shader.setUniform1i("h", h);
    m_shader.setUniform1i("w", w);
    m_shader.setUniform1f("dimmer", dimmer);

    if(autoMix){
        const float timeScale = 0.04;
        m_shaderMix = ofSignedNoise(ofGetElapsedTimef() * timeScale);
    }
    m_shader.setUniform1f("mix", m_shaderMix);

    m_shader.setUniformTexture("tex0", movie["tunnel"]->getTexture(), 1);
    m_shader.setUniformTexture("tex1", movie["tunnel"]->getTexture(), 2);


    movie["tunnel"]->draw(0, 0);
    m_shader.end();

    if(showGui){
        ofSetColor(255);
        const float xOffset = 20;
        const float yOffset = 20;
        ofDrawBitmapString(ofToString(m_shaderMix), xOffset, yOffset);
    }
}

void Tunnel::keyPressed(int key) {
    const float inc = 0.01;
    switch (key) {
        case 'j': {
            m_shaderMix -= inc;
            m_shaderMix = ofClamp(m_shaderMix, -1.0, 1.0);
            break;
        }  
        case 'k': {
            m_shaderMix += inc;
            m_shaderMix = ofClamp(m_shaderMix, -1.0, 1.0);
            break;
        }
        default:
            break;
    }
}

void Tunnel::play() {
    movie["tunnel"]->play();
}

void Tunnel::pause() {
    movie["tunnel"]->setPaused(/*bPause=*/true);
}

void Tunnel::toggleState(){}

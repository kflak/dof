#pragma once

#include "ofMain.h"
#include "Configuration.h"
#include "Scene.h"

class Tunnel: public Scene {
public:

    void setup() override;
    void update() override;
    void draw() override;
    void keyPressed(int key) override;
    void play() override;
    void pause() override;

    void toggleState() override;
    void setMix(float mix) override { m_shaderMix = mix; };

private:
    
    // bool m_autoMix = true;
    const float m_defaultMix = 0.2;
    float m_shaderMix = m_defaultMix;
    float m_smoothingFactor = 0.0;
    ofShader m_shader;
};


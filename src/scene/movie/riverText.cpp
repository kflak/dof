#include "riverText.h"
#include "globalvars.h"
#include "ofTrueTypeFont.h"

void RiverText::setup() {

    const int fontSize = 40;
    ofTrueTypeFontSettings settings("fonts/Akatab-Black.ttf", fontSize);
    settings.addRange(ofUnicode::LatinA);
    settings.addRange(ofUnicode::Latin);
    settings.addRange(ofUnicode::Latin1Supplement);

    m_font.load(settings);
    const float yOffset = 400;
    const float xOffset = 100;
    const float zStartPos = -400;

    m_pos = {ofRandom(0, w - xOffset),
             ofRandom(centerH - yOffset, centerH + yOffset), zStartPos};
}

void RiverText::update() {
    const float zScaled = ofMap(m_pos.z, -400, 2000, 0.0, TWO_PI);
    const int alpha = 180;
    m_alpha = ofClamp(sin(zScaled) * alpha, 0, 255);
    // ofLogNotice() << ofToString(m_alpha);
}

void RiverText::draw() {
    const float r = 0.8;
    const float g = 1.0;
    const float b = 0.9;
    ofSetColor(m_alpha * r, m_alpha * g, m_alpha * b, dimmer * 255);
    ofPushMatrix();
    ofTranslate(m_pos.x, m_pos.y, m_pos.z);

    m_font.drawString(m_text, 0, 0);
    ofPopMatrix();
    m_pos.z += m_speed;
}

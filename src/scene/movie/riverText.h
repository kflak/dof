#include "Scene.h"
#include "ofVec2f.h"

class RiverText : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void setText(string text) { m_text = text; };
    void setSpeed(float speed) { m_speed = speed; };

   private:
    ofTrueTypeFont m_font;
    float m_time0 = ofGetElapsedTimef();
    string m_text = "HELLO WORLD";
    ofVec3f m_pos;
    float m_alpha;
    float m_speed = 2.0;
};

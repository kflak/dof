#pragma once

#include "Configuration.h"
#include "Scene.h"
#include "ofMain.h"
#include "ofUtils.h"
#include "riverText.h"

class River : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void keyPressed(int key) override;
    void play() override;
    void pause() override;

    void toggleState() override;
    void setMix(float mix) override { m_shaderMix = mix; };

   private:
    bool m_autoMix = false;
    const float m_defaultMix = 0.01;
    float m_shaderMix = m_defaultMix;
    float m_smoothingFactor = 0.0;
    ofShader m_shader;
    ofFbo m_fbo;
    vector<string> m_names;
    deque<RiverText> m_text;
    const size_t m_max_names = 200;
    size_t m_nameIdx = 0;
    float m_time = ofGetElapsedTimef();
    float m_interval = 0.8;
    ofSoundPlayer m_sound;
};

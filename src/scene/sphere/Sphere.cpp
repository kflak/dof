#include "Sphere.h"
#include "DotSphere.h"
#include "of3dPrimitives.h"
#include "ofGraphics.h"

void Sphere::setup() {
    m_sphere.clear();
    spawnSphere();

    m_fbo.allocate(w, h);
    m_fbo.begin();
    ofClear(0);
    m_fbo.end();
    m_t0 = ofGetElapsedTimef();
}

void Sphere::spawnSphere() {
    DotSphere sphere;
    sphere.setup();
    m_sphere.push_back(sphere);
    DotSphere::numSpheres = m_sphere.size() + 1;
}

void Sphere::update() {
    m_fbo.begin();
    ofEnableAlphaBlending();
    ofSetColor(0, 0, 0, 15);
    ofDrawRectangle(0, 0, w, h);
    const float now = ofGetElapsedTimef() - m_t0;
    m_numSpheres = ofClamp(floor(now * 0.5), 2, 100);
    if (m_numSpheres != m_prevNumSpheres) {
        spawnSphere();
        m_prevNumSpheres = m_numSpheres;
    }
    for (auto sphere : m_sphere) {
        sphere.draw();
    }

    m_fbo.end();
    ofDisableAlphaBlending();
}

void Sphere::draw() {
    ofSetColor(255);
    m_fbo.draw(0, 0);
    ofEnableAlphaBlending();
    const int opacity = 255 * (1.0 - dimmer);
    ofSetColor(0, 0, 0, opacity);
    ofDrawRectangle(0, 0, w, h);
    ofDisableAlphaBlending();
}

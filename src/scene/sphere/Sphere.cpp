#include "Sphere.h"
#include "of3dPrimitives.h"
#include "ofGraphics.h"

void Sphere::setup() {
    m_sphere.clear();
    m_color.clear();
    m_radius.clear();
    setupSpheres(m_numSpheres);

    m_fbo.allocate(w, h);
    m_fbo.begin();
    ofClear(0);
    m_fbo.end();
    m_t0 = ofGetElapsedTimef();
}

void Sphere::setupSpheres(size_t numSpheres) {
    for (size_t i = 0; i < numSpheres; i++) {
        const float radius = ofMap(i, 0, numSpheres, 0.2, 0.8);
        addSphere(radius);
        const ofColor color =
            ofColor::fromHsb(ofMap(i, 0, numSpheres, 100, 250), 255, 255, 255);
        addColor(color);
        const float dotRadius = ofMap(i, 0, numSpheres, 1, 2);
        addRadius(dotRadius);
    }
}

void Sphere::addSphere(float radius) {
    ofIcoSpherePrimitive sphere;
    sphere.setRadius(centerH * radius);
    sphere.setPosition(centerW, centerH, 0);
    m_sphere.push_back(sphere);
    const ofColor color = ofColor::fromHsb(ofRandom(0, 255), 255, 255, 255);
    addColor(color);
    const float dotRadius = ofRandom(1, 4);
    addRadius(dotRadius);
}

void Sphere::addColor(ofColor color) {

    m_color.push_back(color);
}

void Sphere::addRadius(float radius) {

    m_radius.push_back(radius);
}

void Sphere::update() {
    m_fbo.begin();
    ofEnableAlphaBlending();
    ofSetColor(0, 0, 0, 15);
    ofDrawRectangle(0, 0, w, h);
    const float now = ofGetElapsedTimef() - m_t0;
    m_numSpheres = ofClamp(floor(now * 0.5), 2, 100);
    if (m_numSpheres != m_prevNumSpheres) {
        addSphere(ofRandom(100, centerH));
        m_prevNumSpheres = m_numSpheres;
    }
    for (size_t i = 0; i < m_numSpheres; i++) {
        const int direction = (i % 2 * 2) - 1;
        float radius = sin(now * 0.2 + (i * 1000));
        radius = ofMap(radius, -1.0, 1.0, 100, centerH * 0.8);
        m_sphere[i].setRadius(radius);

        drawSphere(m_sphere[i], direction, m_radius[i], m_color[i]);
    }

    m_fbo.end();
    ofDisableAlphaBlending();
}

void Sphere::draw() {
    ofSetColor(255);
    m_fbo.draw(0, 0);
}

void Sphere::drawSphere(ofIcoSpherePrimitive &sphere, int direction,
                        float radius, ofColor color) {

    auto verts = sphere.getMesh().getVertices();

    ofPushMatrix();
    ofTranslate(centerW, centerH);
    ofRotateYDeg(ofGetElapsedTimef() * ofRandom(25.92, 26) * direction);
    ofRotateXDeg(ofGetElapsedTimef() * ofRandom(14.92, 15) * direction);
    ofRotateZDeg(ofGetElapsedTimef() * ofRandom(19.92, 20) * direction);
    for (auto vert : verts) {
        // ofColor color =
        // ofColor::fromHsb(ofMap(vert.z, -100, 100, 240, 250), 255, 255);
        ofSetColor(color);
        ofDrawCircle(vert, radius);
    }
    ofPopMatrix();
}

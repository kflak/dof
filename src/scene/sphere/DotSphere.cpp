#include "DotSphere.h"
#include "globalvars.h"
#include "ofGraphics.h"

int DotSphere::numSpheres = 0;

void DotSphere::setup() {
    const float speed = ofRandom(0.1, 0.4);
    m_sphere.setRadius(calcRadius(speed));
    m_sphere.setPosition(centerW, centerH, 0);

    m_hue = ofRandom(0, 255);
    m_sat = 255;
    m_brightness = 255;
    m_alpha = 0;

    m_dotRadius = ofRandom(2, 4);
    const array<int, 2> directions = {-1, 1};
    const size_t index = ofRandom(2);
    m_direction = directions[index];
    m_XSpeed = ofRandom(14, 15);
    m_YSpeed = ofRandom(24, 25);
    m_ZSpeed = ofRandom(19, 20);
    m_noise = 0.08;
    m_tOffset = ofRandom(1000, 10000);
    m_t0 = ofGetElapsedTimef() + m_tOffset;
    m_radialSpeed = ofRandom(0.1, 0.6);
}

float DotSphere::calcRadius(float speed = 0.4) {
    float radius = sin(now() * m_radialSpeed);
    radius =
        ofMap(radius, -1.0, 1.0, m_minRadius * centerH, m_maxRadius * centerH);
    return radius;
}

int DotSphere::getAlpha() {
    const int speed = 20;
    const int fade = now() * speed;
    int alpha = min(fade, 255);
    return alpha;
}

float DotSphere::now() {
    float t = ofGetElapsedTimef() + m_tOffset - m_t0;
    return t;
}

void DotSphere::draw() {
    const float speed = 0.1;
    m_sphere.setRadius(calcRadius(speed));

    auto verts = m_sphere.getMesh().getVertices();

    ofPushMatrix();

    ofTranslate(centerW, centerH);
    // float now = ofGetElapsedTimef() + m_tOffset - m_t0;

    const float yRot =
        now() * ofRandom(m_YSpeed - m_noise, m_YSpeed) * m_direction;
    const float xRot =
        now() * ofRandom(m_XSpeed - m_noise, m_XSpeed) * m_direction;
    const float zRot =
        now() * ofRandom(m_ZSpeed - m_noise, m_ZSpeed) * m_direction;

    ofRotateYDeg(yRot);
    ofRotateXDeg(xRot);
    ofRotateZDeg(zRot);
    ofEnableAlphaBlending();
    const int alpha = getAlpha();
    const ofColor color = ofColor::fromHsb(m_hue, m_sat, m_brightness, alpha);

    ofSetColor(color);

    for (const auto &vert : verts) {
        ofDrawCircle(vert, m_dotRadius);
    }

    ofPopMatrix();
    ofDisableAlphaBlending();
}

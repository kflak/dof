#pragma once
#include "ofMain.h"

class DotSphere {
   public:
    void setup();
    void update();
    void draw();
    void setRadius(float radius) { m_radius = radius; }
    void setDotRadius(float radius) { m_dotRadius = radius; }
    void setColor(ofColor color) { m_color = color; }

   private:
    ofIcoSpherePrimitive m_sphere;
    ofColor m_color;
    float m_radius;
    float m_dotRadius;
}

#pragma once
#include "ofMain.h"

class DotSphere {
   public:
    void setup();
    void draw();
    void setRadius(float radius) { m_radius = radius; }
    void setDotRadius(float radius) { m_dotRadius = radius; }
    void setColor(ofColor color) { m_color = color; }
    static int numSpheres;

   private:
    ofIcoSpherePrimitive m_sphere;
    ofColor m_color;
    float m_radius;
    float m_dotRadius;
    int m_direction;
    float m_XSpeed;
    float m_YSpeed;
    float m_ZSpeed;
    float m_noise;
    float m_minRadius = 0.1;
    float m_maxRadius = 0.8;
    float calcRadius(float speed);
    float m_tOffset;
    float m_t0;
    float now();
    float m_radialSpeed;
    int m_hue;
    int m_sat;
    int m_brightness;
    int m_alpha;
    int getAlpha();
};

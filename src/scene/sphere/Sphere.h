#pragma once

#include "DotSphere.h"
#include "Scene.h"
#include "globalvars.h"
#include "of3dPrimitives.h"
#include "ofMain.h"

class Sphere : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void spawnSphere();

   private:
    size_t m_numSpheres = 2;
    size_t m_prevNumSpheres = m_numSpheres;
    vector<DotSphere> m_sphere;
    ofFbo m_fbo;
    float m_t0;
};

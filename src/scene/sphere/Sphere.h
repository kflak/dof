#pragma once

#include "Scene.h"
#include "globalvars.h"
#include "of3dPrimitives.h"
#include "ofMain.h"

class Sphere : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void drawSphere(ofIcoSpherePrimitive &sphere, int direction, float radius,
                    ofColor color);
    void setupSpheres(size_t numSpheres);
    void addSphere(float radius);
    void addColor(ofColor color);
    void addRadius(float radius);

   private:
    size_t m_numSpheres = 2;
    size_t m_prevNumSpheres = m_numSpheres;
    vector<ofIcoSpherePrimitive> m_sphere;
    vector<ofColor> m_color;
    vector<float> m_radius;
    ofFbo m_fbo;
    float m_t0;
};

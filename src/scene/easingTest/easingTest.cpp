#include "easingTest.h"
#include "ofxEasing.h"

void EasingTest::setup() {
    ofSetBackgroundColor(0);
    positions.resize(5);
    initTime = ofGetElapsedTimef();
    endPosition = w - 40;

    easingNames = {
        "linear", "quad", "cubic", "bounce", "back",
    };
}

void EasingTest::update() {
    auto duration = 10.F;
    auto endTime = initTime + duration;
    auto now = ofGetElapsedTimef();
    positions[0] = ofxeasing::map_clamp(now, initTime, endTime, 0, endPosition,
                                        &ofxeasing::linear::easeIn);
    positions[1] = ofxeasing::map_clamp(now, initTime, endTime, 0, endPosition,
                                        &ofxeasing::quad::easeIn);
    positions[2] = ofxeasing::map_clamp(now, initTime, endTime, 0, endPosition,
                                        &ofxeasing::cubic::easeIn);
    positions[3] = ofxeasing::map_clamp(now, initTime, endTime, 0, endPosition,
                                        &ofxeasing::bounce::easeOut);

    // the back easing equation can receive an extra parameter in the _s functions
    // that controls how much the easing goes forward or backwards
    positions[4] = ofxeasing::map_clamp(now, initTime, endTime, 0, endPosition,
                                        &ofxeasing::back::easeOut_s, 0.8);
}

void EasingTest::draw() {
    ofSetColor(255);
    auto h = 20;
    auto y = 20;
    auto i = 0;
    for (auto &x : positions) {
        ofDrawRectangle(0, y, x, h);
        ofDrawBitmapString(easingNames[i], 10, y + h * 1.6);
        y += h * 2;
        i++;
    }

    ofSetColor(200);
    ofDrawLine(endPosition, 0, endPosition, ofGetHeight());
}

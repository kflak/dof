#pragma once
#include "Scene.h"
#include "ofMain.h"

class EasingTest : public Scene {
   public:
    void setup();
    void update();
    void draw();

    std::vector<float> positions;
    std::vector<std::string> easingNames;
    float initTime;
    float endPosition;
};

#include "Concentric.h"
#include "mathHelpers.h"
#include "ofGraphics.h"

void Concentric::setup() {
    m_circles.clear();
    for (size_t i = 0; i < m_numCircles; i++) {
        ConcCircle circle;
        const float innerCircle = 20.0;
        const float radius =
            (static_cast<float>(i) * static_cast<float>(centerH) /
             static_cast<float>(m_numCircles) * 0.8) +
            innerCircle;
        const ofColor color =
            ofColor::fromHsb(ofMap(i, 0, m_numCircles, 255, 200), 255, 255);
        circle.setup(ofVec2f(0, 0), radius, color);
        const float speed = 0.05;
        circle.setSpeed(i * speed);
        m_circles.push_back(circle);
    }
    m_fbo.allocate(w, h, GL_RGBA);
    m_fbo.begin();
    ofClear(0);
    m_fbo.end();
    m_blur.setup(w, h, m_blurRadius, m_blurShape, m_blurNumPasses);
}

void Concentric::update() {
    m_fbo.begin();
    // m_blur.begin();
    ofEnableAlphaBlending();
    ofSetColor(0, 0, 0, 10);
    ofDrawRectangle(0, 0, w, h);
    // ofClear(0);
    ofSetColor(255);
    ofNoFill();
    for (auto circle : m_circles) {
        circle.draw();
    }
    // m_blur.end();
    // m_blur.draw();
    m_fbo.end();
}

void Concentric::draw() {
    ofSetColor(255);
    m_blur.begin();
    m_fbo.draw(0, 0);

    // ofNoFill();

    // for (auto circle : m_circles) {
    //     circle.draw();
    // }

    m_blur.end();
    // m_blur.draw();
    m_fbo.draw(0, 0);
    ofEnableAlphaBlending();
    const int opacity = 255 * (1 - dimmer);
    ofSetColor(0, 0, 0, opacity);
    ofDrawRectangle(0, 0, w, h);
    ofDisableAlphaBlending();
}

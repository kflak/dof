#include "ConcCircle.h"
#include "globalvars.h"
#include "ofGraphics.h"

void ConcCircle::setup(ofVec2f startPos = {0, 0}, float radius = 20,
                       ofColor color = {255, 255, 255}) {
    m_pos = startPos;
    m_color = color;
    m_radius = radius;
    m_t0 = ofGetElapsedTimef();
}

void ConcCircle::update() {}

void ConcCircle::draw() {
    ofSetCircleResolution(100);
    ofSetColor(m_color);
    ofPushMatrix();
    ofTranslate(centerW, centerH);
    const auto now = ofGetElapsedTimef() - m_t0;
    ofRotateXDeg(now * m_rotationSpeed);
    ofDrawCircle(m_pos, m_radius);
    ofPopMatrix();
}

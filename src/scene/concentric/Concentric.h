#pragma once

#include "Scene.h"
#include "concentric/ConcCircle.h"
#include "globalvars.h"
#include "ofMain.h"
#include "ofxBlur.h"

class Concentric : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void setNumCircles(size_t numCircles) { m_numCircles = numCircles; }
    auto getNumCircles() const { return m_numCircles; }
    void setMaxRadius(size_t maxRadius) { m_maxRadius = maxRadius; }
    auto getMaxRadius() const { return m_maxRadius; }

   private:
    vector<ConcCircle> m_circles;
    vector<ofColor> m_colors;
    const size_t m_defaultNumCircles = 100;
    size_t m_numCircles = m_defaultNumCircles;
    size_t m_maxRadius = centerH;
    ofxBlur m_blur;
    ofFbo m_fbo;
    const int m_blurRadius = 2;
    const float m_blurShape = 0.2;
    const int m_blurNumPasses = 5;
    const float m_maxBlurScale = 10.0;
};

#pragma once
#include "ofMain.h"

class ConcCircle {
   public:
    void setup(ofVec2f startPos, float radius, ofColor color);
    void update();
    void draw();

    void setSpeed(float speed) { m_rotationSpeed = speed; }

    auto getRadius() const { return m_radius; }
    auto getColor() const { return m_color; }
    auto getPos() const { return m_pos; }
    auto getZrotation() const { return m_zRotation; }

   private:
    ofVec2f m_pos;
    ofColor m_color;
    float m_radius;
    float m_zRotation = 0;
    float m_yRotation = 0;
    float m_rotationSpeed = 100;
    float m_t0;
};

#include "CamEdge.h"
#include "globalvars.h"

void CamEdge::setup(){
    m_fbo.allocate(w, h, GL_RGB32F_ARB);
    m_blur.setup(w, h, m_blurRadius, m_blurShape, m_blurNumPasses);
    m_image.allocate(w, h);
    m_grayImage.allocate(w, h);
    m_grayBg.allocate(w, h);
    m_grayDiff.allocate(w, h);
}

void CamEdge::update(){

    bool bNewFrame = false;

    webcam.update();
    bNewFrame = webcam.isFrameNew();

    if (bNewFrame) {

        m_pixels = webcam.getPixels();
        ofImage tmpImg;
        tmpImg.setFromPixels(m_pixels);
        tmpImg.resize(w, h);
        m_image = tmpImg;
        m_grayImage = m_image;
        if(m_learnBackground){
            m_grayBg = m_grayImage;
            m_learnBackground = false;
        }

        m_grayDiff.absDiff(m_grayBg, m_grayImage);
        m_grayDiff.threshold(m_threshold);

        const int minArea = 90;
        const int maxArea = 1500;
        const bool bFindHoles = true;
        const int nConsidered = 10;

        m_contourFinder.findContours(m_grayDiff, minArea, maxArea, nConsidered, bFindHoles);
    }

    m_fbo.begin();

    // trails
    ofEnableAlphaBlending();

    alpha = (1 - trailHistory) * 255;
    ofSetColor(0, 0, 0, alpha);
    ofFill();
    ofDrawRectangle(0, 0, w, h);

    ofDisableAlphaBlending();

    ofFill();

    const ofColor lineColor = ofColor::lightGoldenRodYellow;
    ofSetColor(lineColor);
    const int lineWidth = 4;
    ofSetLineWidth(lineWidth);

    vector<ofPolyline> polyline;
	for (int i = 0; i < m_contourFinder.nBlobs; i++){
        ofPolyline blob = m_contourFinder.blobs[i].pts;
        polyline.push_back(blob);
        polyline[i].draw();
	}

    m_fbo.end();
}

void CamEdge::draw(){

    ofSetColor(255, 255, 255, dimmer * 255);

    m_blur.begin();
    m_fbo.draw(0, 0);
    m_blur.end();
    m_blur.draw();
    

    if(showGui){
        const int xOffset = 20;
        const int yOffset = 20;
        ofDrawBitmapString("fps: " + ofToString(ofGetFrameRate()), xOffset, yOffset);
        ofDrawBitmapString("dimmer: " + ofToString(dimmer), 20, yOffset + 20);
        ofDrawBitmapString("threshold: " + ofToString(m_threshold), 20, yOffset + 40);
    }
}

void CamEdge::keyPressed(int key) {
    const int maxThresh = 255;
    const int minThresh = 0;
    const float increment = 0.01;
    switch (key) {
        case ' ': {
            m_learnBackground = true;
            break;
        }
        case '+': {
            m_threshold++;
            if(m_threshold > maxThresh) { m_threshold = maxThresh; }
            break;
        }
        case '-': {
            m_threshold --;
            if (m_threshold < minThresh) { m_threshold = minThresh; }
            break;
        }
        case 'j': {
            trailHistory -= increment;
            break;
        }
        case 'k': {
            trailHistory += increment;
            break;
        }
        default: {}
    }
}

void CamEdge::learnBackground() {
    m_learnBackground = true;
}

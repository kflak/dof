#pragma once
#include "ofMain.h"

class CamEdgeParticle {
public:
    void setup(ofVec2f pos, int radius);
    void update();
    void draw();
    void setColor(ofColor);
    inline auto getColor() { return m_color; };
    inline auto getX() const { return m_pos.x; };
    inline auto getY() const { return m_pos.y; };
    inline auto getPos() const { return m_pos; };
    void setX(int x){ m_pos.x = x; };
    void setY(int y){ m_pos.y = y; };
    void setPos(ofVec2f pos){ m_pos = pos; };
    void setRadius(int radius){ m_radius = radius; }
    void setSpeed(ofVec2f speed){ m_speed = speed; }
    void setAcceleration(ofVec2f acc) { m_acceleration = acc; }
    void setWobble(float min, float max) {m_minWobble = min, m_maxWobble = max;}
    void setMaxSpeed(float maxSpeed){ m_maxSpeed = maxSpeed;}

private:
    ofColor m_color;
    ofVec2f m_pos = {0, 0};
    const int m_defaultRadius = 2;
    int m_radius = m_defaultRadius;
    const float m_defaultSpeedX = 0;
    const float m_defaultSpeedY = 0;
    ofVec2f m_speed = {m_defaultSpeedX, m_defaultSpeedY};
    const float m_defaultAccX = 0.0001;
    const float m_defaultAccY = 0.0001;
    ofVec2f m_acceleration = {m_defaultAccX, m_defaultAccY};
    ofVec2f m_wobble = {0.0, 0.0};
    float m_minWobble = 0.0;
    float m_maxWobble = 0.0;
    float m_maxSpeed = 10.0;
};

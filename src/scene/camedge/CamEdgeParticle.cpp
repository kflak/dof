#include "CamEdgeParticle.h"


void CamEdgeParticle::setColor(ofColor color){
    m_color = color;
}

void CamEdgeParticle::setup(ofVec2f pos, int radius = 3){
    m_pos = pos;
    m_radius = radius;
}

void CamEdgeParticle::update(){
    m_wobble = {ofRandom(m_minWobble, m_maxWobble), ofRandom(m_minWobble, m_maxWobble)};
    m_speed += m_acceleration;
    m_speed += m_wobble;
    m_speed.limit(m_maxSpeed);
    m_pos += m_speed;
}

void CamEdgeParticle::draw(){
    ofDrawCircle(m_pos, m_radius);
}

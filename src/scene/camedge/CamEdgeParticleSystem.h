#pragma once

#include "cameraDeltaTrigger.h"
#include "ofMain.h"
#include "ofxOpenCv.h"
#include "Configuration.h"
#include "Scene.h"
#include "ofxBlur.h"
#include "CamEdgeParticle.h"

class CamEdgeParticleSystem : public Scene {

public:
    void setup() override;
    void update() override;
    void draw() override;

    void keyPressed(int key) override;
    void learnBackground();
    void snapshot(const vector<ofPolyline>& polylines);
    void toggleState() override;
    void setAcceleration(ofVec2f acc) override;
    void setCameraDeltaThreshold(float threshold) override;
    void setTrailHistory(float trail) override {trailHistory = trail;};

private: 
    void m_toggleParticles();
    
    const float m_defaultHistory = 0.99;

public:
    float trailHistory = m_defaultHistory;
    float alpha;

private:
    ofPixels m_pixels;
    ofxCvColorImage m_image;
    vector<ofPolyline> m_polylines;

    ofxCvGrayscaleImage	m_grayImage;
    ofxCvGrayscaleImage	m_grayBg;
    ofxCvGrayscaleImage	m_grayDiff;
    ofxCvContourFinder m_contourFinder;

    const float m_initThreshold = 80;
    float m_threshold = m_initThreshold;
    bool m_learnBackground = true;
    int m_camwidth;
    int m_camheight;
    
    ofFbo m_fbo;
    ofxBlur m_blur;
    const int m_blurRadius = 5;
    const float m_blurShape = 0.2;
    const int m_blurNumPasses = 5;
    const float m_maxBlurScale = 10.0;

    bool m_particlesActive = false;
    vector<CamEdgeParticle> m_particles;
    const int m_defaultRadius = 3;
    int m_radius = 3;
    float m_hueOffset = 0;
    float m_sat = 255;
    float m_brightness = 255;
    float m_timeOfLastSnapshot;
    const float m_flashDur = 0.5;
    ofPixels m_snapPixels;
    ofImage m_snap;
    const ofColor m_defaultBaseColor = ofColor::lightGoldenRodYellow;
    // const ofColor m_defaultBaseColor = ofColor::fromHex(0x121254);
    ofColor m_baseColor = m_defaultBaseColor;
    ofColor m_backgroundColor = ofColor::black;
    // ofColor m_backgroundColor = ofColor::mintCream;
    ofColor m_rotatedColor = m_defaultBaseColor;

    void m_fadeInOut(float timeSinceLastSnapshot );
    double m_prevFadeValue = 0; 
    bool m_hasPeaked = false;
    CameraDeltaTrigger m_cameraDeltaTrigger;
    float m_camDeltaThreshold = 0.03;
    float m_speedlim = 1;
};

#include "CamEdgeParticleEmitter.h"
#include "globalvars.h"

void CamEdgeParticleEmitter::setup(){
    m_fbo.allocate(w, h, GL_RGB32F_ARB);
    m_blur.setup(w, h, m_blurRadius, m_blurShape, m_blurNumPasses);
    m_image.allocate(w, h);
    m_grayImage.allocate(w, h);
    m_grayBg.allocate(w, h);
    m_grayDiff.allocate(w, h);
    m_fbo.begin();
    ofSetColor(m_backgroundColor);
    ofDrawRectangle(0, 0, w, h);
    m_fbo.end();
    m_cameraDeltaTrigger.setup(m_camDeltaThreshold, m_speedlim);
}

void CamEdgeParticleEmitter::update(){

    webcam.update();

    if(enableCameraDelta) {
        if(m_cameraDeltaTrigger.getTrigger()) {
            toggleState();
        }
    }

    bool bNewFrame = false;
    bNewFrame = webcam.isFrameNew();
    if (bNewFrame ) {

        m_pixels = webcam.getPixels();
        ofImage tmpImg;
        tmpImg.setFromPixels(m_pixels);
        tmpImg.resize(w, h);
        m_image = tmpImg;
        m_grayImage = m_image;
        if(m_learnBackground){
            m_grayBg = m_grayImage;
            m_learnBackground = false;
        }

        m_grayDiff.absDiff(m_grayBg, m_grayImage);
        m_grayDiff.threshold(m_threshold);

        const int minArea = 90;
        const int maxArea = 1500;
        const bool bFindHoles = false;
        const int nConsidered = 5;

        m_contourFinder.findContours(m_grayDiff, minArea, maxArea, nConsidered, bFindHoles);
    }

    // START DRAWING
    m_fbo.begin();
    // trails
    ofEnableAlphaBlending();

    alpha = (1 - trailHistory) * 255;
    ofSetColor(m_backgroundColor, alpha);
    ofFill();
    ofDrawRectangle(0, 0, w, h);

    ofDisableAlphaBlending();

    ofFill();

    const float colorSpeed = 1.0;

    const int maxColorValue = 255;
    m_hueOffset += colorSpeed;
    if(m_hueOffset < 0){m_hueOffset = maxColorValue;}
    if(m_hueOffset > maxColorValue){m_hueOffset = 0;}
    char hue = static_cast<char>(m_hueOffset);


    m_rotatedColor = ofColor::fromHsb(m_baseColor.getHue() + hue, m_baseColor.getSaturation(), m_baseColor.getBrightness());

    ofSetColor(m_rotatedColor);
    for(auto & particle : m_particles) {
        particle.update();
        particle.draw();
    }
    const ofColor color = m_baseColor;
    ofSetColor(color);

    m_polylines.clear();
    for (size_t i = 0; i < m_contourFinder.nBlobs; i++){
        ofPolyline blob = m_contourFinder.blobs[i].pts;
        m_polylines.push_back(blob);
        m_polylines[i].draw();
    }

    // ofEnableAlphaBlending();
    // float timeSinceLastSnapshot = ofGetElapsedTimef() - m_timeOfLastSnapshot;
    // if(timeSinceLastSnapshot < m_flashDur){
    //     m_fadeInOut( timeSinceLastSnapshot );
    // } 

    // m_hasPeaked = false;
    // trailHistory = m_defaultHistory;

    // ofDisableAlphaBlending();

    m_fbo.end();

}

void CamEdgeParticleEmitter::draw(){

    ofSetColor(255);

    m_blur.begin();
    m_fbo.draw(0, 0);
    m_blur.end();
    m_blur.draw();


    if(showGui){
        ofSetColor(0);
        const int xOffset = 20;
        const int yOffset = 20;
        ofDrawBitmapString("fps: " + ofToString(ofGetFrameRate()), xOffset, yOffset);
        ofDrawBitmapString(ofToString(dimmer), 20, yOffset + 20);
    }
}

void CamEdgeParticleEmitter::keyPressed(int key) {
    const int maxThresh = 255;
    const int minThresh = 0;
    const float increment = 0.01;
    switch (key) {
        case ' ': {
                      m_learnBackground = true;
                      break;
                  }
        case '+': {
                      m_threshold++;
                      if(m_threshold > maxThresh) { m_threshold = maxThresh; }
                      break;
                  }
        case '-': {
                      m_threshold --;
                      if (m_threshold < minThresh) { m_threshold = minThresh; }
                      break;
                  }
        case 'j': {
                      trailHistory -= increment;
                      break;
                  }
        case 'k': {
                      trailHistory += increment;
                      break;
                  }
        case 't': {
                      toggleState();
                      break;
                  }
        default: {}
    }
}

void CamEdgeParticleEmitter::learnBackground() {
    m_learnBackground = true;
}

void CamEdgeParticleEmitter::snapshot(const vector<ofPolyline>& polylines){
    m_particles.clear();
    m_hueOffset = 0;

    // ofTexture tex = m_fbo.getTexture();
    // tex.readToPixels(m_snapPixels);
    // for(unsigned char & pix : m_snapPixels) {
    //     pix = 255 - pix;
    // };

    // m_snap = m_snapPixels;

    for(const auto & polyline : polylines) {
        const size_t vecsize = 3;
        vector<glm::vec<vecsize, float>> vertices = polyline.getVertices();
        for(size_t i = 0; i < polyline.size(); i++){

            ofVec3f vertex = vertices[i];
            CamEdgeParticle particle;
            const ofVec2f pos(vertex[0], vertex[1]);

            const ofVec2f tangent = polyline.getTangentAtIndexInterpolated(i);
            const ofVec2f acc = tangent * 0.005;
            particle.setup(pos, m_radius);
            particle.setAcceleration(acc);

            const float maxWobble = 0.05;
            particle.setWobble(-maxWobble, maxWobble);

            const float maxSpeed = 1;
            particle.setMaxSpeed(maxSpeed);

            m_particles.emplace_back(particle);
        };
    }
    // m_particlesActive = true;
    m_timeOfLastSnapshot = ofGetElapsedTimef();
}

void CamEdgeParticleEmitter::m_toggleParticles(){
    m_particlesActive = !m_particlesActive;
}

void CamEdgeParticleEmitter::toggleState(){
    if (m_particlesActive){
        m_particlesActive = false;
        m_baseColor = m_rotatedColor;
    } else {
        snapshot(m_polylines);
    }
}

void CamEdgeParticleEmitter::m_fadeInOut(float timeSinceLastSnapshot ){
    const double fadeCurve = sin(timeSinceLastSnapshot / (m_flashDur / PI)); 
    const double delta = fadeCurve - m_prevFadeValue;
    const bool hasPeaked = delta < 0;

    if(hasPeaked && !m_hasPeaked){
        m_particlesActive = true;
    }

    m_prevFadeValue = fadeCurve;
    trailHistory = m_defaultHistory * (1 - fadeCurve);

    const double opacity  = fadeCurve * 255 * dimmer;
    ofSetColor(m_backgroundColor, static_cast<int>(opacity));
    ofDrawRectangle(0, 0, w, h);
}

void CamEdgeParticleEmitter::setAcceleration(const ofVec2f acc){
    for(auto & particle : m_particles) {
        particle.setAcceleration(acc);
    }
}

void CamEdgeParticleEmitter::setCameraDeltaThreshold(const float threshold){
    m_threshold = threshold;
}

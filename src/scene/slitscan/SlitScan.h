#pragma once

#include "ofMain.h"
#include "Scene.h"
#include "globalvars.h"
#include "ofxBlur.h"

class SlitScan : public Scene {
public:

    SlitScan();

    void update() override;
    void draw() override;

    void keyPressed(int key) override;

    void play() override;
    void pause() override;

    int hue;
    int sat;
    int val;
    
    float blurScale = 2.0;

private:
    void calcPixels();

    deque<ofPixels> m_frames;
    const int m_bufferSize = 100;

    ofPixels m_imagePixels;
    ofImage m_image;

    ofColor m_getSlitPixelColor(int x, int y);

    ofxBlur m_blur;
    const int m_blurRadius = 10;
    const float m_blurShape = 0.2;
    const int m_blurNumPasses = 4;
    const float m_maxBlurScale = 10.0;


    // ofVideoGrabber m_video;
    // const int m_camWidth = 640;
    // const int m_camHeight = 480;


    const float m_xOffset = 0.16;
    const float m_yOffset = 0.22;

    int m_stepsize = 1;

    // int m_blurMBid;

    ofFbo m_fbo;

    static constexpr int defaultHue = 125;
    static constexpr int defaultSat = 255;
    static constexpr int defaultVal = 255;
};

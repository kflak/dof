#include "SlitScan.h"

SlitScan::SlitScan() : hue(defaultHue), sat(defaultSat), val(defaultVal) {

    m_blur.setup(w, h, m_blurRadius, m_blurShape, m_blurNumPasses);
    m_fbo.allocate(w, h, GL_RGB32F_ARB);

    ofSetVerticalSync(/*bSync=*/true);
};

void SlitScan::update() {

    // ofBackground(100, 100, 100);
    webcam.update();

    if (webcam.isFrameNew()) {
        m_frames.push_front(webcam.getPixels());
        if (m_frames.size() > m_bufferSize) {
            m_frames.pop_back();
        }
    }

    if (!m_frames.empty()) {
        if (!m_imagePixels.isAllocated()) {
            m_imagePixels = m_frames[0];
        }
    }

    size_t width = m_frames[0].getWidth();
    size_t height = m_frames[0].getHeight();

    for (size_t y = 0; y < height; y += m_stepsize) {
        for (size_t x = 0; x < width; x += m_stepsize) {
            ofColor color = m_getSlitPixelColor(x, y);
            m_imagePixels.setColor(x, y, color);
        }
    }

    m_blur.setScale(blurScale);

    m_image.setFromPixels(m_imagePixels);
    m_image.resize(w, h);

    m_fbo.begin();
    m_blur.begin();
    ofSetColor(ofColor::fromHsb(hue, sat, dimmer * val));
    m_image.draw(0, 0);
    m_blur.end();
    m_fbo.end();
};

void SlitScan::draw() {

    ofSetColor(255);
    // m_blur.begin();
    m_fbo.draw(0, 0);
    // m_blur.end();

    m_blur.draw();

    if (showGui) {}
};

ofColor SlitScan::m_getSlitPixelColor(int x, int y) {
    float distance = ofDist(x, y, m_image.getWidth() * m_xOffset,
                            m_image.getHeight() * m_yOffset);
    const float divisor = 8.0;
    float frame = distance / divisor;

    //compute two frame numbers surrounding frame
    int in0 = static_cast<int>(frame);
    int in1 = in0 + 1;

    float weight0 = in1 - frame;
    float weight1 = 1 - weight0;

    int n = m_frames.size() - 1;
    in0 = ofClamp(in0, 0, n);
    in1 = ofClamp(in1, 0, n);

    ofColor color0 = m_frames[in0].getColor(x, y);
    ofColor color1 = m_frames[in1].getColor(x, y);

    ofColor color = color0 * weight0 + color1 * weight1;

    return color;
}

void SlitScan::keyPressed(int key) {}

void SlitScan::play() {}
void SlitScan::pause() {}

#pragma once

#include "ofMain.h"
#include "Configuration.h"
#include "Scene.h"

class CamFire: public Scene {
public:

    void setup() override;
    void update() override;
    void draw() override;
    void keyPressed(int key) override;
    void play() override;
    void pause() override;

    void toggleState() override;
private:
    const float m_defaultRotation = 1.0;

public:
    float rotation = m_defaultRotation;

private:
    ofShader m_shader;
    ofImage m_fireplaceimg;
    ofPixels m_pixels;
    ofImage m_image;
    const float m_red = 1.0;
    const float m_green = 1.0;
    const float m_blue = 1.0;
    
};


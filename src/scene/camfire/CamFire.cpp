#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "CamFire.h"

void CamFire::setup() {

    m_shader.load("shader/glitch");
    m_fireplaceimg.load("movie/fireplace.png");

    ofSetVerticalSync(/*bSync=*/true);
}

void CamFire::update() {
    movie["fireplace"]->update();
    webcam.update();
    m_pixels = webcam.getPixels();
    m_image.setFromPixels(m_pixels);
    m_image.setImageType(OF_IMAGE_GRAYSCALE);
    m_image.resize(w, h);
}


void CamFire::draw() {


    m_shader.begin();
    m_shader.setUniform1i("h", h);
    m_shader.setUniform1i("w", w);
    m_shader.setUniform1f("dimmer", dimmer);
    m_shader.setUniform1f("red", m_red);
    m_shader.setUniform1f("green", m_green);
    m_shader.setUniform1f("blue", m_blue);

    // const float rotSpeed = 0.1;
    // const float rotMul = 4.0;
    // rotation = ofNoise(ofGetElapsedTimef() * rotSpeed) * rotMul;
    m_shader.setUniform1f("rotation", rotation);

    if(movie["fireplace"]->getTexture().isAllocated()){
        m_shader.setUniformTexture("tex0", movie["fireplace"]->getTexture(), 1);
    } else {
        m_shader.setUniformTexture("tex0", m_fireplaceimg.getTexture(), 1);
    }
    m_shader.setUniformTexture("tex1", m_image.getTexture(), 2);

    const ofColor color = ofColor::lightGoldenRodYellow;
    ofSetColor(color);

    movie["fireplace"]->draw(0, 0);
    m_shader.end();
}

void CamFire::keyPressed(int key) {
}

void CamFire::play() {
    movie["fireplace"]->play();
}

void CamFire::pause() {
    movie["fireplace"]->setPaused(/*bPause=*/true);
}

void CamFire::toggleState(){}


#include "stick.h"
#include "mathHelpers.h"
#include "ofGraphics.h"

void Stick::setup(ofVec2f start=ofVec2f(0, 0), ofVec2f end=ofVec2f(10, 10), ofColor color=ofColor::white) {
    m_start = start;
    m_end = end;
    m_color = color;
    for(size_t i = 0; i < m_numSticks; i++) {
        m_startPos.push_back(polarToCartesian({m_start.x + i*4, m_start.y}));
        m_endPos.push_back(polarToCartesian({m_end.x + i*4, m_end.y}));
    }
}

void Stick::update() { 
    float time = ofGetElapsedTimef();
    const float alpha = pow((m_lifeTime - (time - m_creationTime)) / m_lifeTime, 0.3) * 255;
    m_color.setBrightness(alpha);
    if (time - m_creationTime > m_lifeTime) {
        m_isAlive = false;
    } else {
        for(size_t i = 0; i < m_numSticks; i++) {
            m_startPos[i] += m_startSpeed + i * m_startSpread;
            m_endPos[i] += m_endSpeed + i * m_endSpread;
        }
    }
}

void Stick::draw() {
    ofEnableAlphaBlending();
    ofSetColor(m_color);
    for(size_t i = 0; i < m_numSticks; i++) {
        ofVec2f start = m_startPos[i];
        ofVec2f end = m_endPos[i];
        ofDrawLine(start, end);
    }
    ofDisableAlphaBlending();
}

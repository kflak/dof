#pragma once
#include "Scene.h"
#include "stick.h"

class Sticks : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void createSticks(size_t numSticks) override;
    void play() override;
    void pause() override;

   private:
    vector<Stick> m_sticks;
    float m_creationTime;
    ofColor m_color;
    float m_colorIncrement = 1.4;
    float m_hue = 0;
    float m_timeToNextSticks = 3;
    ofImage m_image;
    ofFbo m_fbo;
    ofShader m_shader;
    bool m_createSticksAutomatically = false;
};

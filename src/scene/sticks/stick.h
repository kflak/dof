#pragma once
#include "ofMain.h"

class Stick {
   public:
    void setup(ofVec2f start, ofVec2f end, ofColor color);
    void update();
    void draw();
    void setStart(ofVec2f start){ m_start = start; };
    void setEnd(ofVec2f end){ m_end = end; };
    void setColor(ofColor color){ m_color = color; };
    void setWidth(float width){ m_width = width; };
    void setLifeTime(float lifeTime){ m_lifeTime = lifeTime; };
    void setStartSpeed(ofVec2f startSpeed){ m_startSpeed = startSpeed; };
    void setEndSpeed(ofVec2f endSpeed){ m_endSpeed = endSpeed; };
    bool isAlive() const{ return m_isAlive; };

   private:
    ofVec2f m_start;
    ofVec2f m_end;
    ofColor m_color;
    float m_width;
    bool m_isAlive = true;
    float m_lifeTime = 10;
    float m_creationTime = ofGetElapsedTimef();
    vector<ofVec2f> m_startPos;
    vector<ofVec2f> m_endPos;
    ofVec2f m_startSpeed = {0.0, 0.0};
    ofVec2f m_endSpeed = {0.0, 0.0};
    size_t m_numSticks = ofRandom(2, 8);
    float m_spread = ofRandom(0.2, 2);
    float m_startSpread = ofRandom(-m_spread, m_spread);
    float m_endSpread = ofRandom(-m_spread, m_spread);
};

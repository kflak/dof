#include "sticks.h"
#include "mathHelpers.h"

void Sticks::setup() {
    m_color = ofColor::fromHsb(0, 255, 255);
    createSticks(4);
    m_image.load("image/vignette1.png");
    m_fbo.allocate(w, h);
    m_shader.load("shader/mask");
}

void Sticks::update() {
    movie["snow"]->update();
    float time = ofGetElapsedTimef();
    if(time - m_creationTime > m_timeToNextSticks && m_createSticksAutomatically) {
        const size_t numSticks = ofRandom(1, 10);
        createSticks(numSticks);
        m_timeToNextSticks = ofRandom(3, 15);
    }
    for(auto& stick : m_sticks) {
        stick.update();
    }

    m_fbo.begin();

    ofClear(0);
    movie["snow"]->draw(0, 0, w, h);
    ofEnableAlphaBlending();
    const float alpha = 0.7;
    ofSetColor(0, 0, 0, alpha * 255);
    ofDrawRectangle(0, 0, w, h);
    ofDisableAlphaBlending();
    ofPushMatrix();
    ofTranslate(centerW, centerH);
    ofNoFill();
    const float angle = 180;
    ofColor color = m_color;
    color.setHueAngle(m_color.getHueAngle() + angle);
    const float brightness = 200;
    const size_t numCircles = m_sticks.size() * 2;
    for (size_t i = 0; i < numCircles; i++) {
        color.setBrightness(brightness * i / numCircles);
        ofSetColor(color);
        const float radius = ofMap(i, 0, numCircles, 0, centerH);
        ofDrawCircle(0, 0, radius);
    }
    for (size_t i = 0; i < m_sticks.size(); i++) {
        auto *stick = &m_sticks[i];
        if(!stick->isAlive()) {
            m_sticks.erase(m_sticks.begin() + i);
        } else {
            stick->draw();
        }
    }
    ofPopMatrix();
    m_fbo.end();
}

void Sticks::draw() {
    m_shader.begin();
    m_shader.setUniformTexture("tex0", m_fbo.getTexture(), 1);
    m_shader.setUniformTexture("tex1", m_image.getTexture(), 2);
    m_shader.setUniform1f("dimmer", dimmer);
    m_image.draw(0, 0);
    m_fbo.draw(0, 0);
    m_shader.end();
}

void Sticks::createSticks(size_t numSticks) {
    m_creationTime = ofGetElapsedTimef();
    const size_t maxSticks = 50;
    for (size_t i = 0; i < numSticks; i++) {
        m_hue += m_colorIncrement;
        m_color.setHue(m_hue);
        Stick stick;
        
        const ofVec2f start = {static_cast<float>(centerH),
                               ofRandom(0, 2 * PI)};
        const ofVec2f end = {static_cast<float>(centerH), ofRandom(0, 2 * PI)};
        const ofVec2f startSpeed = {
            ofRandom(-0.1, ofMap(numSticks, 0, maxSticks, -0.1, -7)),
            ofMap(numSticks, 0, maxSticks, 0.001, 0.01)};
        const ofVec2f endSpeed = {ofMap(numSticks, 0, maxSticks, -0.1, -1),
                                  ofMap(numSticks, 0, maxSticks, 0.001, 0.04)};
        // const ofVec2f startSpeed = {ofRandom(-5, -0.1), ofRandom(0.01)};
        // const ofVec2f endSpeed = {ofRandom(-5, -0.1), ofRandom(0.01)};
        stick.setup(start, end, m_color);
        stick.setStartSpeed(startSpeed);
        stick.setEndSpeed(endSpeed);
        m_sticks.push_back(stick);
    }
}

void Sticks::play() {
    movie["snow"]->play();
}

void Sticks::pause() {
    movie["snow"]->setPaused(/*bPause=*/true); 
}

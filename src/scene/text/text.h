#include "ofMain.h"
#include "Scene.h"
#include "movingRectangle.h"

class Text : public Scene {
public:

    void setup() override;
    void update() override;
    void draw() override;
    int fadeInOut(float elapsedTime) const;
    void resetTime() override { m_time0 = ofGetElapsedTimef(); };

private:
    
    ofFbo m_textBuffer;
    ofPixels m_textPixels;
    ofTrueTypeFont m_font;
    ofBuffer m_text;
    vector<string> m_lines;
    bool m_isTextActive = true;
    size_t m_lineIndex = 0;
    float m_time0 = ofGetElapsedTimef();
    const float m_timeToSwitchText = 8;
    const size_t m_numRectangles = 10;
    vector<MovingRectangle> m_rectangles;
};


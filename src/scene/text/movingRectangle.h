#include "ofMain.h"


class MovingRectangle {
public:
    MovingRectangle();
    ~MovingRectangle();

    void setup(ofVec2f pos, float w, float h);
    void update();
    void draw();
    void setX(float x){m_pos.x = x;}
    void setY(float y){m_pos.y = y;}
    void setXSpeed(float xSpeed){m_speed.x = xSpeed;}
    void setYSpeed(float ySpeed){m_speed.y = ySpeed;}
    void setColor(ofColor color){m_color = color;}
    void setW(float w){m_w = w;}
    void setH(float h){m_h = h;}

private:
    ofVec2f m_pos = {0, 0};
    ofVec2f m_speed = {0, 0};
    const float m_defaultW = 100;
    const float m_defaultH = 100;
    float m_w = m_defaultW;
    float m_h = m_defaultH;
    ofColor m_color = ofColor::white;
};

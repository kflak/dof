#include "scene/text/instructions.h"

void Instructions::setup() {
    const int fontSize = 120;
    // m_font.load("font/1968odyssey.ttf", fontSize);
    // https://fontstruct.com/fontstructions/show/15956/steadelijk
    m_font.load("font/steadelijk.ttf", fontSize);
    // m_font.load("font/MUSICNET.ttf", fontSize);
    m_text = ofBufferFromFile("instructions.txt");
    m_lines = ofSplitString(m_text.getText(), "\n");
    ofSetCircleResolution(100);
    m_textBuffer.allocate(w, h);
    m_shader.load("shader/mask");
}

void Instructions::update() {

    m_textBuffer.begin();
    ofClear(0);
    auto color = ofColor(255);
    ofSetColor(color);
    const string str = m_lines[m_lineIndex];
    const float stringWidth = m_font.stringWidth(str);
    const float stringHeight = m_font.stringHeight(str);
    const int offset = 150;
    const int yOffset = -centerH + stringHeight + offset;

    ofPushMatrix();
    ofTranslate(centerW, centerH);
    ofRotateZRad(m_increment);
    for(int i = 0; i < 4; i++) {
        ofRotateZRad(i * PI/2);
        m_font.drawString(str, stringWidth / -2, yOffset);
    }
    ofDrawCircle(0, 0, 200);
    ofSetColor(0);
    ofDrawCircle(0, 0, 20);
    ofSetColor(color);
    ofNoFill();
    ofDrawCircle(0, 0, centerH);
    ofPopMatrix();
    m_increment += m_incrementSpeed;
    m_textBuffer.end();

    float time = ofGetElapsedTimef() + 10;
    if(time > m_timeToNextChange && m_randomizeText){
        toggleState();
        m_timeToNextChange = time + ofRandom(10);
        auto color = ofColor::fromHsb(ofRandom(255), 255, 255);
        m_red = color.r / 255.0;
        m_green = color.g / 255.0;
        m_blue = color.b / 255.0;
    }

    movie["fireplace"]->update();
    m_pixels = movie["fireplace"]->getPixels();
    m_image.setFromPixels(m_pixels);
}

void Instructions::draw() {
    m_shader.begin();
    m_shader.setUniformTexture("tex0", m_textBuffer.getTexture(), 1);
    m_shader.setUniformTexture("tex1", m_image.getTexture(), 2);
    m_shader.setUniform1f("dimmer", dimmer);
    m_shader.setUniform1f("red", m_red);
    m_shader.setUniform1f("green", m_green);
    m_shader.setUniform1f("blue", m_blue);
    m_shader.setUniform1f("multiply", m_multiply);
    m_shader.setUniform1f("add", m_add);
    m_textBuffer.draw(0, 0);
    m_image.draw(0, 0);
    m_shader.end();
}

void Instructions::toggleState() {
    m_lineIndex = ofRandom(m_lines.size());
}

void Instructions::randomize() {
    
}


void Instructions::play() {
    movie["fireplace"]->play();
}

void Instructions::pause() {
    movie["fireplace"]->setPaused(/*bPause=*/true);
}

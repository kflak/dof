#pragma once
#include "Scene.h"

class Instructions : public Scene {
   public:

    void setup() override;
    void draw() override;
    void update() override;
    void toggleState() override;
    void randomize() override;
    void play() override;
    void pause() override;

private:
    ofTrueTypeFont m_font;
    ofBuffer m_text;
    vector<string> m_lines;
    size_t m_lineIndex = 0;
    float m_timeToNextChange = ofRandom(10);
    bool m_randomizeText = false;
    float m_increment = 0;
    const float m_incrementSpeed = 0.002;
    ofFbo m_textBuffer;
    ofShader m_shader;
    ofPixels m_pixels;
    ofImage m_image;
    float m_red = 0.6;
    float m_green = 1.0;
    float m_blue = 0.4;
    float m_multiply = 2.0;
    float m_add = 0.3;
};

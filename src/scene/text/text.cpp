#include "text.h"

void Text::setup() {

    m_textBuffer.allocate(w, h);
    const int fontSize = 40;
    m_font.load("font/Akatab-Black.ttf", fontSize);
    m_text = ofBufferFromFile("haraway.txt");
    m_lines = ofSplitString(m_text.getText(), "\n");

    for (size_t i = 0; i < m_numRectangles; i++) {
        MovingRectangle rect = MovingRectangle();

        const float opacity = 60;
        ofColor color = ofColor(255, 255, 255, opacity);
        rect.setColor(color);

        rect.setXSpeed(ofRandom(-2, 2));

        const float x = ofRandom(w / 2);
        const float y = 0;

        const float minW = 100;
        const float maxW = w / 2;
        const float width = ofRandom(minW, maxW);

        rect.setup({x, y}, width, h);

        m_rectangles.push_back(rect);
    }
}

void Text::update() {

    ofSetColor(255);
    m_textBuffer.begin();
    ofClear(0);

    const int yOffset = 800;
    const float time1 = ofGetElapsedTimef();
    float elapsedTime = time1 - m_time0;
    if (elapsedTime > m_timeToSwitchText && m_isTextActive) {
        m_time0 = time1;
        m_lineIndex++;
        // hack to make sure there's no flicker at
        // the beginning of a cycle.
        elapsedTime = 0;
    }
    if (m_lineIndex < m_lines.size()) {
        const float color = fadeInOut(elapsedTime);
        ofSetColor(255, 255, 255, color);
        const string str = m_lines[m_lineIndex];
        const float stringWidth = m_font.stringWidth(str);
        const float rightEdge = 1800;
        const float justifyRight = rightEdge - stringWidth;
        m_font.drawString(str, justifyRight, yOffset);
    } else {
        m_isTextActive = false;
    }
    for (auto &rect : m_rectangles) {
        rect.update();
        rect.draw();
    }

    m_textBuffer.end();
}

void Text::draw() {
    ofSetColor(255, 255, 255, dimmer * 255);
    m_textBuffer.draw(0, 0);
    if (showGui) {
        ofDrawBitmapString(ofToString(dimmer), 20, 20);
    }
}

int Text::fadeInOut(float elapsedTime) const {
    const double dur = m_timeToSwitchText / PI;
    const double fadeCurve = sin(elapsedTime / dur);
    const int value = ofClamp(fadeCurve * 255 * 2, 0, 255);
    return value;
}

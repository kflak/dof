#include "movingRectangle.h"
#include "globalvars.h"


MovingRectangle::MovingRectangle() = default;
MovingRectangle::~MovingRectangle() = default;

void MovingRectangle::setup(ofVec2f pos, float w, float h){
    m_pos = pos;
    m_w = w;
    m_h = h;
}

void MovingRectangle::update(){
    if(m_pos.x < 0 || m_pos.x > w) { m_speed = -m_speed; }
    m_pos += m_speed;
}

void MovingRectangle::draw(){
    ofEnableAlphaBlending();
    ofSetColor(m_color);
    ofDrawRectangle(m_pos, m_w, m_h);
    ofDisableAlphaBlending();
}

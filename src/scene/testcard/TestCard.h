#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "Scene.h"

/// \class test image
/// \brief draws a small frame around both display areas
class TestCard: public Scene {
public:
	virtual void draw();

private:
	void drawBoundingBox(const string &txt);
};

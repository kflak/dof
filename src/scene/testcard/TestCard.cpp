#include "TestCard.h"

void TestCard::drawBoundingBox(const string &txt) {
    ofClear(0);
    ofDrawBitmapStringHighlight(txt, 200, 200);
}

void TestCard::draw() {
    drawBoundingBox("TestCard");
}

#pragma once

#include "Scene.h"
enum {
    Spiral,
    NonSpiral
};

class CircleDisplay : public Scene {
   public:
    void setup() override;
    void update() override;
    void draw() override;
    void setSource(const string& source) override {
        m_source = source;
        if(isMovie(m_source)){
            movie[m_source]->play();
        }
    }
    void play() override;
    void pause() override;
    void toggleState() override;
    void setRotationSpeed(float speed) override {
        m_rotationSpeed = speed;
    }

private:
    const float m_circleSize = 20.0;
    string m_source = "webcam";
    ofPixels m_pixels;
    ofImage m_img;
    const size_t m_numRings = h / 2 / m_circleSize;
    size_t m_state = Spiral;
    const float m_defaultSpeed = 0.01;
    float m_rotationSpeed = m_defaultSpeed;
};

#include "circleDisplay.h"

void CircleDisplay::setup() {
    m_img.allocate(w, h, OF_IMAGE_COLOR);
}

void CircleDisplay::update() {
    bool bNewFrame = false;

    if (m_source == "webcam") {
        webcam.update();
        bNewFrame = webcam.isFrameNew();
        if (bNewFrame) {
            m_pixels = webcam.getPixels();
            m_img.setFromPixels(m_pixels);
            m_img.resize(w, h);
        }
    } else {
        if (isMovie(m_source)) {
            movie[m_source]->update();
        }
        bNewFrame = movie[m_source]->isFrameNew();
        if (bNewFrame) {
            m_pixels = movie[m_source]->getPixels();
            m_img.setFromPixels(m_pixels);
            m_img.resize(w, h);
        }
    }
}

void CircleDisplay::draw() {

    ofSetBackgroundColor(0);

    vector <ofVec2f>xyPoints;
    float time = ofGetElapsedTimef();

    for (size_t i = 0; i < m_numRings; i++) {
        const float y = i * m_circleSize;
        ofPushMatrix();
        ofTranslate(centerW, centerH);
        const float circumference = PI * 2 * y;
        const size_t numCircles = circumference / m_circleSize;
        for (size_t j = 0; j < numCircles; j++) {
            const float x = j * m_circleSize;
            const float rotation = PI * 2 / numCircles * j;
            ofPushMatrix();
            ofRotateZRad(rotation);
            if(m_state == Spiral){
                const ofColor color = m_img.getColor(x, y);
                ofSetColor(color);
                const float minRadius = 0.001;
                float radius = ofClamp(m_circleSize / 2 * ofNoise(time * j * m_rotationSpeed), minRadius, m_circleSize);
                ofDrawCircle(0, y, radius);
            }
            ofPopMatrix();
            if(m_state == NonSpiral){
                ofVec2f xy;
                xy.x = centerW + (glm::sin(rotation)*y);
                xy.y = centerH + (glm::cos(rotation)*y);
                xyPoints.push_back(xy);
            }
        }
        ofPopMatrix();
    }
    if(m_state == NonSpiral){
        for(size_t i = 0; i < xyPoints.size(); i++){
            const ofColor color = m_img.getColor(xyPoints[i].x, xyPoints[i].y);
            const float minRadius = 0.001;
            float radius = ofClamp(m_circleSize / 2 * ofNoise(time * i * m_rotationSpeed), minRadius, m_circleSize);
            ofSetColor(color);
            ofDrawCircle(xyPoints[i], radius);
        }
    }
}

void CircleDisplay::play() {
    if (isMovie(m_source)) {
        movie[m_source]->play();
    }
}

void CircleDisplay::pause() {
    if (isMovie(m_source)) {
        movie[m_source]->setPaused(true);
    }
}

void CircleDisplay::toggleState() {
    m_state = (m_state + 1) % 2;
}

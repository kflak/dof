#pragma once

#include <utility>

#include "Configuration.h"
#include "Scene.h"
#include "cameraDeltaTrigger.h"
#include "ofMain.h"
#include "ofxKaleidoscope.h"

class Caleidoscope : public Scene {

   public:
    void setup() override;
    void update() override;
    void draw() override;
    void play() override;
    void pause() override;
    void setHeight(size_t height) override { m_height = height; }
    void keyPressed(int key) override;
    void setRotation(float rotation) override { m_rotation = rotation; }
    void setSegments(size_t seg) override { m_segments = seg > 4 ? seg : 2; }
    void setSource(const string& source) override {
        m_source = source;
        if(isMovie(m_source)){
            movie[m_source]->play();
        }
    }

   private:
    int m_camwidth;
    int m_camheight;

    ofPixels m_pixels;
    ofImage m_img;

    ofxKaleidoscope m_kScope;
    size_t m_height = h;
    const size_t m_defaultSegments = 80;
    size_t m_segments = m_defaultSegments;
    float m_defaultRotation = -0.01;
    float m_rotation = m_defaultRotation;
    float m_rad = 0.0;
    string m_source = "webcam";
    ofFbo m_fbo;
    ofImage m_vignette;
    ofShader m_shader;
    CameraDeltaTrigger m_cameraDeltaTrigger;
};

#include "caleidoscope.h"
#include "globalvars.h"

void Caleidoscope::setup() {
    m_img.allocate(w, h, OF_IMAGE_COLOR);
    m_kScope.setup(w, h);
    m_fbo.allocate(w, h);
    m_vignette.load("image/vignette1.png");
    m_shader.load("shader/mask");
    const float camDTThreshold = 0.02;
    const float camDTSpeedlim = 1;
    m_cameraDeltaTrigger.setup(camDTThreshold, camDTSpeedlim);
}

void Caleidoscope::update() {

    bool bNewFrame = false;

    if(enableCameraDelta) {
        if(m_cameraDeltaTrigger.getTrigger()){
            const size_t minSegments = 4;
            const size_t maxSegments = 200;
            m_segments = ofRandom(minSegments, maxSegments);
        }
    }

    if(m_source == "webcam"){
        webcam.update();
        bNewFrame = webcam.isFrameNew();

        if (bNewFrame) {
            m_pixels = webcam.getPixels();
            m_img.setFromPixels(m_pixels);
            m_img.resize(w, h);
        }
    } else {
        if(isMovie(m_source)){
            movie[m_source]->update();
        }
    }
    m_kScope.setSegments(m_segments);
    m_fbo.begin();
    ofClear(0);
    ofEnableAlphaBlending();
    ofSetColor(255, 255, 255, dimmer * 255);

    if(m_source != "webcam"){
        m_kScope.draw(movie[m_source]->getTexture(), m_height, m_rad);
    } else {
        m_kScope.draw(m_img.getTexture(), m_height, m_rad);
    }

    m_rad += m_rotation;

    ofDisableAlphaBlending();
    m_fbo.end();
}

void Caleidoscope::draw() {

    m_shader.begin();
    m_shader.setUniformTexture("tex0", m_fbo.getTexture(), 1);
    m_shader.setUniformTexture("tex1", m_vignette.getTexture(), 2);
    m_shader.setUniform1f("dimmer", dimmer);
    m_vignette.draw(0, 0);
    m_fbo.draw(0, 0);
    m_shader.end();

    if (showGui) {
        const int xOffset = 20;
        const int yOffset = 20;
        ofDrawBitmapString("fps: " + ofToString(ofGetFrameRate()), xOffset,
                           yOffset);
    }
}

void Caleidoscope::keyPressed(int key) {
    const int increment = 10;
    switch (key) {
        case OF_KEY_UP: {
            m_height += increment;
            break;
        }
        case OF_KEY_DOWN: {
            m_height -= increment;
            break;
        }
        case 'k': {
            m_segments += 2;
            break;
        }
        case 'j': {
            if (m_segments > 4) {
                m_segments -= 2;
            }
            break;
        }
        default:

            break;
    }
}

void Caleidoscope::play(){
    if(isMovie(m_source)){
        movie[m_source]->play();
    }
}

void Caleidoscope::pause(){
    if(isMovie(m_source)){
        movie[m_source]->setPaused(true);
    }
}

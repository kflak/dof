#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "Scene.h"

/// \class both windows completely black
class Black: public Scene {
public:
	Black();
	virtual ~Black();
	virtual void draw();
    void play() override;
    void pause() override;
    void toggleState() override;
};

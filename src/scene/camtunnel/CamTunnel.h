#pragma once

#include "ofMain.h"
#include "Configuration.h"
#include "Scene.h"

class CamTunnel: public Scene {
public:

    void setup() override;
    void update() override;
    void draw() override;
    void keyPressed(int key) override;
    void play() override;
    void pause() override;

    // void toggleState() override;
    void setMix(float mix) override { m_shaderMix = mix;};
    void resetTime() override { m_time0 = ofGetElapsedTimef(); };
    // int fadeInOut(float elapsedTime) const;
    void setLag(float lag) override { m_lag = lag; };

private:
    
    // bool m_autoMix = true;
    const float m_defaultMix = 0.2;
    float m_shaderMix = m_defaultMix;

    ofShader m_shader;
    ofPixels m_pixels;
    ofImage m_image;
    // ofFbo m_textBuffer;
    // ofPixels m_textPixels;
    // ofTrueTypeFont m_font;
    // ofBuffer m_text;
    // vector<string> m_lines;
    float m_time0 = ofGetElapsedTimef();
    // const float m_timeToSwitchText = 5;
    // size_t m_lineIndex = 0;
    // bool m_isTextActive = true;
    const float m_defaultLag = 0.99;
    float m_lag = m_defaultLag;
    float m_prevMix = m_shaderMix;
};

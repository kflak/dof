#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "CamTunnel.h"

void CamTunnel::setup() {
    m_shader.load("shader/displace");
    // m_textBuffer.allocate(w, h);
    // const int fontSize = 40;
    // m_font.load("Akatab-Black.ttf", fontSize);
    // m_text = ofBufferFromFile("haraway.txt");
    // m_lines = ofSplitString(m_text.getText(), "\n");
}

void CamTunnel::update() {
    movie["tunnel"]->update();

    webcam.update();
    m_pixels = webcam.getPixels();
    m_image.setFromPixels(m_pixels);
    m_image.resize(w, h);
}


void CamTunnel::draw() {

    // ofSetColor(0);
    m_shader.begin();

    m_shader.setUniform1i("h", h);
    m_shader.setUniform1i("w", w);
    m_shader.setUniform1f("dimmer", dimmer);

    if(autoMix){
        const float timeScale = 0.04;
        m_shaderMix = ofSignedNoise(ofGetElapsedTimef() * timeScale);
    }

    m_shaderMix = m_shaderMix + m_lag * (m_prevMix - m_shaderMix);
    m_prevMix = m_shaderMix;

    m_shader.setUniform1f("mix", m_shaderMix);

    m_shader.setUniformTexture("tex1", movie["tunnel"]->getTexture(), 2);
    m_shader.setUniformTexture("tex0", m_image.getTexture(), 1);
    // m_shader.setUniformTexture("tex2", m_textBuffer.getTexture(), 3);


    movie["tunnel"]->draw(0, 0);
    m_shader.end();

    if(showGui){
        ofSetColor(255);
        const float xOffset = 20;
        const float yOffset = 20;
        ofDrawBitmapString(ofToString(m_shaderMix), xOffset, yOffset);
    }
}

void CamTunnel::keyPressed(int key) {
    const float inc = 0.01;
    switch (key) {
        case 'j': {
            m_shaderMix -= inc;
            m_shaderMix = ofClamp(m_shaderMix, -1.0, 1.0);
            break;
        }  
        case 'k': {
            m_shaderMix += inc;
            m_shaderMix = ofClamp(m_shaderMix, -1.0, 1.0);
            break;
        }
        default:
            break;
    }
}

void CamTunnel::play() {
    movie["tunnel"]->play();
}

void CamTunnel::pause() {
    movie["tunnel"]->setPaused(/*bPause=*/true);
}

// void CamTunnel::toggleState(){}

// int CamTunnel::fadeInOut(float elapsedTime) const{
//     const double dur = m_timeToSwitchText / PI;
//     const double fadeCurve = sin(elapsedTime / dur); 
//     const int value = ofClamp(fadeCurve * 255 * 2, 0, 255);
//     return value;
// }

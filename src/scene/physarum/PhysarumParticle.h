#pragma once

#include "Configuration.h"
#include "globalvars.h"
#include "ofMain.h"

enum PhysarumParticleMode {
    PHYS_CIRCLE,
    PHYS_HORIZON,
    PHYS_NOISE,
    PHYS_RANDOMHORIZON,
    PHYS_VERTICAL,
    PHYS_RANDOMVERTICAL,
    PHYS_DONUT,
};

enum PhysarumWrapMode {
    PHYS_WRAP_ON,
    PHYS_WRAP_SHIFT_X,
    PHYS_WRAP_OFF,
    PHYS_WRAP_CIRCLE,
    PHYS_WRAP_DONUT
};

class PhysarumParticle {
   public:
    PhysarumParticle();
    ofVec2f pos;
    ofVec2f dir;
    int pw = 1;
    int ph = 1;

    void setup(
        int mode = PHYS_CIRCLE, int w_ = 1, int h_ = 1, float startX = 0.5,
        float startY = 0.5,
        int spread =
            20);  // startX and startY as ratios of screen widht and height
    void deposit(unsigned char *pixel_ptr, size_t pix_w, size_t pix_h,
                 size_t pix_channels);
    void deposit(ofPixels &trailmap);
    void sense(unsigned char *pixel_ptr, size_t pix_w, size_t pix_h,
               size_t pix_channels);
    void sense(ofPixels &trailmap);
    void move();
    void wrap();
    void wrapShiftX();
    void wrapCircle();
    void wrapDonut();
    void reset();

    void setMaxSpeed(float n);
    void setMaxAngle(float n);
    void setDepositAmt(float n);
    void setSenseDist(float n);
    void setNumDirs(int n);

    int wrapMode = PHYS_WRAP_ON;

    float randomHeight = 0.1;
    float randomWidth = 0.1;
    float innerDiameter = centerH / 2;
    float outerDiameter = centerH;

   private:
    float heading = 0;
    float maxSpeed = 0;
    float maxAngle = 0;
    float depositAmt = 0;
    float senseDist = 0;
    int numDirs = 32;
    int padding = 0;
};

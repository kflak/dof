#pragma once

#include "Physarum.h"

class CleanPhysarum : public Physarum {

   public:
    CleanPhysarum();
    void setParticleMode(size_t mode) override { particleMode = mode; };

   private:
};

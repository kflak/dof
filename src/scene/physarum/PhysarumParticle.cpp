#include "PhysarumParticle.h"
#include "globalvars.h"
#include "mathHelpers.h"
#include "ofVec2f.h"

PhysarumParticle::PhysarumParticle() {}

void PhysarumParticle::setup(int mode, int w_, int h_, float startX_,
                             float startY_, int spread_) {
    pw = w_;
    ph = h_;
    padding = ceil(senseDist);
    int sp = spread_;

    int xpos, ypos;

    switch (mode) {
        case PHYS_CIRCLE: {
            float distance = ofRandom(sp);
            float angle = ofRandom(2 * std::numbers::pi);
            float xOffset = cos(angle) * distance;
            float yOffset = sin(angle) * distance;
            float sx = startX_;
            float sy = startY_;
            xpos = pw * sx + xOffset;
            ypos = ph * sy + yOffset;
            break;
        }
        case PHYS_DONUT: {
            float distance = ofRandom(innerDiameter, sp);
            float angle = ofRandom(2 * std::numbers::pi);
            float xOffset = cos(angle) * distance;
            float yOffset = sin(angle) * distance;
            float sx = startX_;
            float sy = startY_;
            xpos = pw * sx + xOffset;
            ypos = ph * sy + yOffset;
            break;
        }
        case PHYS_HORIZON: {
            float height = startY_ + (ofRandom(-sp, sp) / ph);
            xpos = ofRandom(0, pw) - padding;
            ypos = ph * height;
            ypos = ofClamp(ypos, 0, ph - padding);
            break;
        }
        case PHYS_RANDOMHORIZON: {
            xpos = ofRandom(0, pw) - padding;
            ypos = ph * randomHeight;
            break;
        }
        case PHYS_VERTICAL: {
            float width = startX_;
            xpos = ofRandom(0, pw) - padding;
            ypos = ph * width;
            break;
        }
        case PHYS_RANDOMVERTICAL: {
            xpos = pw * randomWidth;
            ypos = ofRandom(0, ph) - padding;
            break;
        }
        case PHYS_NOISE: {
            xpos = ofRandom(0, pw) - padding;
            ypos = ofRandom(0, ph) - padding;
            break;
        }
    }

    pos.set(xpos, ypos);
    heading = floor(ofRandom(numDirs));

    depositAmt = 170;
    maxSpeed = 7.8;
    senseDist = 9.2;
    maxAngle = 360.f;
}

inline static int clamp_pos(float value, int limit) {
    if (value <= 0.0)
        return 0;
    if (value >= limit)
        return limit - 1;
    return (int)value;
}

void PhysarumParticle::deposit(ofPixels &trailmap) {
    const int clamped_x = clamp_pos(pos.x, pw);
    const int clamped_y = clamp_pos(pos.y, ph);
    float intensity = trailmap.getColor(clamped_x, clamped_y).getBrightness();
    intensity += depositAmt;
    intensity = ofClamp(intensity, 0, 255);
    ofColor c = ofColor(intensity, 0);
    trailmap.setColor(clamped_x, clamped_y, c);
}

void PhysarumParticle::deposit(unsigned char *pixel_ptr, size_t pix_w,
                               size_t pix_h, size_t pix_channels) {
    const int clamped_x = clamp_pos(pos.x, pix_w);
    const int clamped_y = clamp_pos(pos.y, pix_h);
    const size_t pdi = (clamped_x + clamped_y * pix_w) * pix_channels;
    float intensity =
        (pixel_ptr[pdi] + pixel_ptr[pdi + 1] + pixel_ptr[pdi + 2]) / 3.0f;
    intensity += depositAmt;
    intensity = ofClamp(intensity, 0, 255);
    pixel_ptr[pdi] = intensity;
    pixel_ptr[pdi + 1] = intensity;
    pixel_ptr[pdi + 2] = intensity;
}

void PhysarumParticle::sense(ofPixels &trailmap) {
    padding = ceil(senseDist);
    float nextIntensity = 0;
    float maxIntensity = 0;
    float maxHeading = 0;
    for (int i = -1; i < 2; i++) {
        float look = heading + i;
        float angle = look * (maxAngle / numDirs);
        ofVec2f offset(cos(angle), sin(angle));
        offset.scale(senseDist);

        int currentX, currentY;
        currentX = pos.x + offset.x;
        currentY = pos.y + offset.y;

        if (currentX > pw - 1) {
            currentX = padding;
        } else if (currentX < padding) {
            currentX = pw - padding;
        }

        if (currentY > ph - padding) {
            currentY = padding;
        } else if (currentY < padding) {
            currentY = ph - padding;
        }

        const int clamped_x = clamp_pos(currentX, pw);
        const int clamped_y = clamp_pos(currentY, ph);
        nextIntensity = trailmap.getColor(clamped_x, clamped_y).r;

        if (maxIntensity < nextIntensity) {
            maxIntensity = nextIntensity;
            dir.x = offset.x;
            dir.y = offset.y;
            dir.scale(maxSpeed);
            maxHeading = i;
            // TODO: add attractor
        }
    }

    heading += maxHeading;
}

void PhysarumParticle::sense(unsigned char *pixel_ptr, size_t pix_w,
                             size_t pix_h, size_t pix_channels) {
    padding = ceil(senseDist);
    float nextIntensity = 0;
    float maxIntensity = 0;
    float maxHeading = 0;
    for (int i = -1; i < 2; i++) {
        float look = heading + i;
        float angle = look * (maxAngle / numDirs);
        ofVec2f offset(cos(angle), sin(angle));
        offset.scale(senseDist);

        int currentX, currentY;
        currentX = pos.x + offset.x;
        currentY = pos.y + offset.y;

        if (currentX > pw - 1) {
            currentX = padding;
        } else if (currentX < padding) {
            currentX = pw - padding;
        }

        if (currentY > ph - padding) {
            currentY = padding;
        } else if (currentY < padding) {
            currentY = ph - padding;
        }

        const int clamped_x = clamp_pos(currentX, pix_w);
        const int clamped_y = clamp_pos(currentY, pix_h);
        const size_t pdi = (clamped_x + clamped_y * pix_w) * pix_channels;
        //        nextIntensity = pixel_ptr[pdi]; // test only the "red" channel, assume that green and blue have the same value
        nextIntensity = (pixel_ptr[pdi] + pixel_ptr[pdi] + pixel_ptr[pdi]) /
                        3.0f;  // test r, g and b channel

        if (maxIntensity < nextIntensity) {
            maxIntensity = nextIntensity;
            dir.x = offset.x;
            dir.y = offset.y;
            dir.scale(maxSpeed);
            maxHeading = i;
        }
    }

    heading += maxHeading;
}

void PhysarumParticle::move() {
    pos += dir;
    switch (wrapMode) {
        case PHYS_WRAP_ON:
            wrap();
            break;
        case PHYS_WRAP_SHIFT_X:
            wrapShiftX();
            break;
        case PHYS_WRAP_OFF:
            reset();
            break;
        case PHYS_WRAP_CIRCLE:
            wrapCircle();
            break;
        case PHYS_WRAP_DONUT:
            wrapDonut();
            break;
    }
}

void PhysarumParticle::wrap() {
    if (pos.x > pw - 1)
        pos.x = padding;
    if (pos.x < 0)
        pos.x = pw - padding;
    if (pos.y > ph - 1)
        pos.y = padding;
    if (pos.y < 0)
        pos.y = ph - padding;
}

void PhysarumParticle::wrapShiftX() {
    if (pos.x > pw - 1)
        pos.x = padding;
    if (pos.x < 0)
        pos.x = pw - padding;
    if (pos.y > ph - 1) {
        pos.y = padding;
        pos.x = (int(pos.x) + pw / 2) % (pw - padding);
    };
    if (pos.y < 0) {
        pos.y = ph - padding;
        pos.x = (int(pos.x) + pw / 2) % (pw - padding);
    }
}

void PhysarumParticle::wrapCircle() {
    const ofVec2f polar =
        cartesianToPolar(ofVec2f(pos.x - centerW, pos.y - centerH));
    if (polar[0] > centerH - 1) {
        pos.x = centerW;
        pos.y = centerH;
        dir = -dir;
    }
}

void PhysarumParticle::wrapDonut() {
    const ofVec2f polar =
        cartesianToPolar(ofVec2f(pos.x - centerW, pos.y - centerH));
    if (polar[0] > outerDiameter - 1 || polar[0] < innerDiameter) {
        const ofVec2f cart =
            polarToCartesian(ofVec2f(ofRandom(innerDiameter, outerDiameter),
                                     ofRandom(2 * std::numbers::pi)));
        pos.x = cart[0] + centerW;
        pos.y = cart[1] + centerH;
    }
}

void PhysarumParticle::reset() {
    if (pos.x > pw - 1 || pos.x < padding)
        pos.x = ofRandomWidth() - padding;
    if (pos.y > ph - 1 || pos.y < padding)
        pos.y = ofRandomHeight() * 2 - padding;
}

void PhysarumParticle::setMaxSpeed(float n) {
    maxSpeed = n;
}

void PhysarumParticle::setMaxAngle(float n) {
    maxAngle = n;
}

void PhysarumParticle::setSenseDist(float n) {
    senseDist = n;
}

void PhysarumParticle::setDepositAmt(float n) {
    depositAmt = n;
}

void PhysarumParticle::setNumDirs(int n) {
    numDirs = n;
}

#include "CleanPhysarum.h"
#include "PhysarumParticle.h"

CleanPhysarum::CleanPhysarum() {

    particleMode = PHYS_CIRCLE;
    wrap = PHYS_WRAP_CIRCLE;
    hue = 0;
    sat = 255;
    val = 255;
    depositAmt = 255;
    setup();
}

#include "Physarum.h"
#include "globalvars.h"

Physarum::Physarum() : Scene() {}

Physarum::~Physarum() {
    trailmap.clear();
    pixBuffer.clear();
    texBuffer.clear();
}

void Physarum::setup() {

    Scene::setup();

    // w = Configuration::windowWidth;
    // h = Configuration::windowHeight;

    for (auto &particle : p) {
        particle.setup(particleMode, w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y,
                       startX, startY, spread);
        // i.setup(particleMode, w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y, startX, startY, spread);
        particle.wrapMode = wrap;
    }

    trailmap.allocate(w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y, GL_RGB);
    pixBuffer.allocate(w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y,
                       OF_IMAGE_COLOR);
    texBuffer.allocate(w * PHYSARUM_TILES_X, h * PHYSARUM_TILES_Y, GL_RGB);

    shader.load("shader/physarum");

    gui.setup();
    gui.add(numDirs.setup("Number of Directions", 3, 1, 64));
    gui.add(maxAngle.setup("Max Angle", 360.0, 0.0, 360.0));
}

void Physarum::update() {

    if (!hasStarted) {
        startTime = ofGetElapsedTimeMillis();
        hasStarted = true;
    }

    switch (pixBuffer.getPixelFormat()) {
        case OF_PIXELS_UNKNOWN:
        case OF_PIXELS_NATIVE:
            break;
        case OF_PIXELS_RGB:
        case OF_PIXELS_RGBA: {
            //TODO test with RGBA images.
            unsigned char *pixel_ptr = pixBuffer.getData();
            size_t pix_w = pixBuffer.getWidth();
            size_t pix_h = pixBuffer.getHeight();
            size_t pix_channels = pixBuffer.getNumChannels();
            for (auto &i : p) {
                i.setMaxSpeed(maxSpeed);
                i.setMaxAngle(maxAngle);
                i.setNumDirs(numDirs);
                i.setSenseDist(senseDist);
                i.setDepositAmt(depositAmt);
                i.sense(pixel_ptr, pix_w, pix_h, pix_channels);
                i.move();
                i.deposit(pixel_ptr, pix_w, pix_h, pix_channels);
            }
            break;
        }
        default: {
            for (auto &i : p) {
                i.setMaxSpeed(maxSpeed);
                i.setMaxAngle(maxAngle);
                i.setNumDirs(numDirs);
                i.setSenseDist(senseDist);
                i.setDepositAmt(depositAmt);
                i.sense(pixBuffer);
                i.move();
                i.deposit(pixBuffer);
            }
        }
    }

    texBuffer.loadData(pixBuffer);

    trailmap.begin();
    shader.begin();
    shader.setUniformTexture("tex", texBuffer, 0);
    shader.setUniform1f("dR", decayRate);
    shader.setUniform1i("w", w);
    shader.setUniform1i("h", h);
    shader.setUniform1f("strength", std::max(0.0f, strength));
    shader.setUniform1f("scale", scale);
    shader.setUniform1f("off", off);
    shader.setUniform2f("center", centerX, centerY);
    texBuffer.draw(0, 0);
    shader.end();
    trailmap.end();
    trailmap.readToPixels(pixBuffer);
}

void Physarum::drawPolyline() {

    ofSetColor(dimmer * 255);
    float angle = 0;
    while (angle < TWO_PI) {
        line.curveTo(100 * cos(angle), 0, 100 * sin(angle));
        line.curveTo(300 * cos(angle), 300, 300 * sin(angle));
        angle += TWO_PI / 30;
    };
    line.close();
    line.draw();
    ofSetColor(0, 0, 0);
}

void Physarum::draw() {
    ofBackground(0);

    ofSetColor(ofColor::fromHsb(hue, sat, dimmer * val));

    trailmap.draw(0, 0);

    if (showGui) {
        gui.draw();
    }
}

void Physarum::setImage(string p) {
    ofImage img;
    img.load(p);
    img.setImageType(OF_IMAGE_GRAYSCALE);
    img.resize(ofGetWidth(), ofGetHeight());
    pixBuffer = img;
}

void Physarum::resetParticles(int mode = PHYS_CIRCLE) {
    for (auto &i : p) {
        i.setup(mode);
    }
}
void Physarum::setParticleRandomHeight(float h = 0) {
    for (auto &i : p) {
        i.randomHeight = h;
    };
    resetParticles(PHYS_RANDOMHORIZON);
}

void Physarum::setParticleRandomWidth(float w = 0) {
    for (auto &i : p) {
        i.randomWidth = w;
    };
    resetParticles(PHYS_RANDOMVERTICAL);
}

void Physarum::keyPressed(int key) {
    Scene::keyPressed(key);

    switch (key) {
        case 'a':
            resetParticles(PHYS_CIRCLE);
            break;
        case 's':
            resetParticles(PHYS_HORIZON);
            break;
            // case 'd':
            // 	resetParticles(PHYS_BOTTOMLINE);
            // 	break;
            // case 'f':
            // 	resetParticles(PHYS_TOPLINE);
            // 	break;
        case 'g':
            resetParticles(PHYS_NOISE);
            break;
        case 'z':
            setParticleRandomHeight(ofGetHeight() - 10);
            break;
        case 'x':
            setParticleRandomWidth(ofGetWidth() - 10);
            break;
    }
}

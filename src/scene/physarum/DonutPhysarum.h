#pragma once

#include "Physarum.h"

class DonutPhysarum : public Physarum {

   public:
    DonutPhysarum();
    void setParticleMode(size_t mode) override { particleMode = mode; };
};

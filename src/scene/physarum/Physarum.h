// Based on Samuel Cho's (https://github.com/chosamuel) implementation of
// slime mold growth patterns.
// Original repo here: https://github.com/chosamuel/openframeworks-physarum
// further developed by Thomas Jourdan (https://metagrowing.org)

#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxSlider.h"

#include "Configuration.h"
#include "PhysarumParticle.h"
#include "Scene.h"

#define PHYSARUM_TILES_X 1
#define PHYSARUM_TILES_Y 1

/// \class slime mold growth pattern
class Physarum : public Scene {
   public:
    Physarum();
    ~Physarum() override;
    void setup() override;
    void update() override;
    virtual void drawPolyline();
    void draw() override;
    void keyPressed(int key) override;

    void resetParticles(int mode);
    void setParticleRandomHeight(float h);
    void setParticleRandomWidth(float w);
    void setImage(string p);
    void setDepositAmount(float amount) override { depositAmt = amount; };
    void setMaxAngle(float angle) override { maxAngle = angle; };
    void setNumDirections(int numDirections) override {
        numDirs = ofClamp(numDirections, 1, 64);
    };
    void setMaxSpeed(float maxSpeed) override { this->maxSpeed = maxSpeed; };
    void setHSV(float hue, float saturation, float value) override {
        this->hue = hue;
        this->sat = saturation;
        this->val = value;
    };
    void setSenseDistance(float distance) override { senseDist = distance; };
    void setDecayRate(float rate) override { decayRate = rate; }
    void setWrap(int wrapMode) override {
        wrap = wrapMode;
        setup();
    }
    ofShader shader;
    ofFbo trailmap;
    ofPixels pixBuffer;
    ofTexture texBuffer;

    bool useImage = true;
    int wrap = PHYS_WRAP_CIRCLE;

    float threshold = 0.2;
    int speedLim = 1000;

    // static const int numParticles = 30000;
    static const int numParticles = 60000;
    PhysarumParticle p[numParticles];
    int particleMode = PHYS_CIRCLE;

    ofxPanel gui;
    ofxFloatSlider maxAngle;
    ofxIntSlider numDirs;

   protected:
    float hue = 0;
    float sat = 255;
    float val = 255;
    float depositAmt = 178;
    float decayRate = 0.9;
    float prevDecayRate = 0.9;
    float maxSpeed = 4.0;
    float deltaLeft = 0.0;
    float deltaRight = 0.0;
    float senseDist = 5.0;
    float strength = 2.0;
    float scale = 1.0;
    float off = 0.05;
    float centerX = 0.5;
    float centerY = 0.5;

    // used to set up particle position as ratio of screen width/height
    float startX = 0.5;
    float startY = 0.5;
    int spread = 20;

    // int w;
    // int h;

   private:
    int startTime = 0;
    bool hasStarted = false;

    ofPolyline line;
};

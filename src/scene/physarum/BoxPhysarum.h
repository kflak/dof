#pragma once

#include "ofMain.h"

#include "Configuration.h"
#include "Physarum.h"
#include "ScoreLine.h"

/// \class base class for all media streams
/// \brief provides empty default functions for derivative subclasses
class BoxPhysarum : public Physarum {
   public:
    // BoxPhysarum();
    // ~BoxPhysarum() override;
    void setup() override;
    void update() override;
    void draw() override;
    void keyPressed(int key) override;
    void showPos();
    float r = 1.0;
    float g = 1.0;
    float b = 1.0;
    void play() override;
    void pause() override;

   private:
    ofFbo m_lines;
    vector<ScoreLine> m_line;
    ofShader m_shader;
    ScoreLine m_currentLine;
    void m_setDimmerValues();

    const float m_lineWidth = 60;
    const float m_padding = 50;
    const float m_radius = 3.0;
    const float m_physStartY = 0.2;
    const float m_physStartX = 0.2;
    const int m_spread = 20;
    const double m_depositAmt = 255;
    const vector<float> m_offset = {0.0, 3.0, 5.0};
    const float m_centeredLineY = centerH - (m_lineWidth / 2.0);
    const float m_centeredLineW = centerW - (m_lineWidth / 2.0);
    const vector<ofVec2f> m_startPos = {
        ofVec2f(m_padding, m_centeredLineY),
        ofVec2f(m_padding * 20.0, m_centeredLineY - m_padding),
        ofVec2f(w * 0.6, h * 0.75),
    };
    const vector<float> m_speed = {0.003, 0.008, 0.001};
    const vector<int> m_depth = {h, h, w};
    float m_dimmer = 1;
    ofImage m_testImage;
    const float m_numPianoOctaves = 8;
    const float m_pianoWidth = w / m_numPianoOctaves;
    const float m_numClarinetOctaves = 4;
    const float m_clarinetHeight = h / m_numClarinetOctaves;

    const vector<float> m_alphaSpeeds{0.1, 0.14, 0.16};
    const vector<float> m_alphaModSpeeds{0.09, 0.1, 0.11};
};

#include "BoxPhysarum.h"
#include "globalvars.h"
#include "ofMain.h"

// BoxPhysarum::BoxPhysarum() = default;

// BoxPhysarum::~BoxPhysarum() = default;

void BoxPhysarum::setup() {
    
    Physarum::particleMode = PHYS_RANDOMVERTICAL;
    Physarum::spread = m_spread;
    Physarum::wrap = PHYS_WRAP_ON;
    Physarum::startX = setting.getValue("boxphysarum:startX", m_physStartX);
    Physarum::startY = setting.getValue("boxphysarum:startY", m_physStartY);
    Physarum::depositAmt = setting.getValue("boxphysarum:depositAmt", m_depositAmt);
	Physarum::setup();

    m_lines.allocate(w, h, GL_RGB);
    m_lines.begin();
        ofClear(0);
    m_lines.end();

    m_currentLine = ScoreLine();
    m_currentLine.speed = m_speed[0];
    m_currentLine.depth = m_depth[0];
    m_currentLine.offset = m_offset[0];
    m_currentLine.mode = ScoreLinePerlinHorizontal;
    m_currentLine.setup(m_startPos[0], m_pianoWidth, m_lineWidth);
    m_line.push_back(m_currentLine);

    m_currentLine = ScoreLine();
    m_currentLine.speed = m_speed[1];
    m_currentLine.depth = m_depth[1];
    m_currentLine.offset = m_offset[1];
    m_currentLine.mode = ScoreLinePerlinHorizontal;
    m_currentLine.setup(m_startPos[1], m_pianoWidth, m_lineWidth);
    m_line.push_back(m_currentLine);

    const float offset = m_lineWidth / 2.0;
    m_currentLine = ScoreLine();
    m_currentLine.speed = m_speed[2];
    m_currentLine.depth = m_depth[2];
    m_currentLine.offset = m_offset[2];
    m_currentLine.mode = ScoreLinePerlinVertical;
    m_currentLine.setup(m_startPos[2], m_lineWidth, m_clarinetHeight);
    m_line.push_back(m_currentLine);

    m_shader.load("shader/boxPhysarum");

}

void BoxPhysarum::update() {
    const float depositSpeed = 0.01;
    const float depositAmt = 230.0;
    Physarum::depositAmt = ofNoise(ofGetElapsedTimef() * depositSpeed) * depositAmt;

    const float maxSpeedRateofChange = 0.02;
    const float maxSpeedMax = 6.0;
    Physarum::maxSpeed = ofNoise(ofGetElapsedTimef() * maxSpeedRateofChange) * maxSpeedMax;

    const float senseDistSpeed = 0.03;
    const float senseDistMax = 7.0;
    Physarum::senseDist = ofNoise(ofGetElapsedTimef() * senseDistSpeed) * senseDistMax;

    const float decayRateSpeed = 0.05;
    const float decayRateMax = 1.5;
    Physarum::decayRate = ofNoise(ofGetElapsedTimef() * decayRateSpeed) * decayRateMax;

    Physarum::update();


    m_lines.begin();
    m_setDimmerValues();

    ofClear(0);

    for(auto & line : m_line) {
        line.update();
        line.draw();
    }

    m_lines.end();
}

void BoxPhysarum::draw() {
    
    ofBackground(0);

    m_shader.begin();
    m_shader.setUniformTexture("tex0", trailmap.getTexture(), 1);
    // m_shader.setUniformTexture("tex1", m_testImage.getTexture(), 2);
    m_shader.setUniformTexture("tex1", m_lines.getTexture(), 2);
    m_shader.setUniform1f("r", r);
    m_shader.setUniform1f("g", g);
    m_shader.setUniform1f("b", b);
    m_shader.setUniform1f("radius", m_radius);
    m_shader.setUniform1f("dimmer", m_dimmer);
    m_shader.setUniform1i("height", h);
    // m_lines.draw(0, 0);
    trailmap.draw(0, 0);
    m_shader.end();

    const ofColor lineColor = ofColor::gold;
    ofSetColor(lineColor);
    for (size_t i = 0; i <= m_numPianoOctaves; i++) {
        const float x = w * (static_cast<float>(i) / m_numPianoOctaves);
        ofDrawLine(x, 0, x, h);
    }
    for (size_t i = 0; i <= m_numClarinetOctaves; i++) {
        const float y = h * (static_cast<float>(i) / m_numClarinetOctaves);
        ofDrawLine(0, y, w, y);
    }

    if(showGui) {
        gui.draw();
    }

    // const int xPos = 20;
    // const int yPos = 20;
    // ofDrawBitmapString(ofGetFrameRate(), xPos, yPos);

}

void BoxPhysarum::keyPressed(int key) {
    // switch (key) {
    //     case 'n': for(auto line : m_line){
    //                 line.mode = line.mode++;
    //             }
    // }
}

void BoxPhysarum::showPos(){
    const int xPos = 20;
    const int yPos = 20;
    const int inc = 20;
    const int xOffset = 40;

    for (size_t i = 0; i < m_line.size(); i++) {
        ofDrawBitmapString(ofToString(m_line[0].getX()), xPos, yPos + (i*inc));
        ofDrawBitmapString(ofToString(m_line[0].getY()), xPos + xOffset, yPos + (i*inc));
    }
}

void BoxPhysarum::play(){}
void BoxPhysarum::pause(){}

void BoxPhysarum::m_setDimmerValues(){
    for (size_t i = 0; i < m_line.size(); i++) {

        const float alphaSpeedBase = m_alphaSpeeds[i];
        const float alphaSpeedModSpeed = m_alphaModSpeeds[i];
        float alphaSpeedMod = sin(ofGetElapsedTimef() * alphaSpeedModSpeed);
        float alphaSpeed = alphaSpeedBase * alphaSpeedMod;

        const float alpha = ofClamp( cos(ofGetElapsedTimef() * alphaSpeed), 0.0, 1.0);
        const int alphaScaled = alpha * 256;
        m_line[i].alpha = alphaScaled;
    }
}

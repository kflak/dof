#include "DonutPhysarum.h"
#include "PhysarumParticle.h"
#include "ofGraphics.h"

DonutPhysarum::DonutPhysarum() {

    particleMode = PHYS_DONUT;
    wrap = PHYS_WRAP_DONUT;
    hue = 0;
    sat = 255;
    val = 255;
    depositAmt = 255;
    setup();
}

#include "ofMain.h"
#include "Configuration.h"
#include "ofApp.h"
#include "CamGlitch.h"

void CamGlitch::setup() {

    m_shader.load("shader/glitch");

    const float camDTSpeedlim = 0.5;
    const float camDTThreshold = 0.02;
    m_cameraDeltaTrigger.setup(camDTThreshold, camDTSpeedlim);
}

void CamGlitch::update() {
    webcam.update();
    m_pixels = webcam.getPixels();
    m_image.setFromPixels(m_pixels);
    m_image.resize(w, h);

    if(enableCameraDelta){
        if(m_cameraDeltaTrigger.getTrigger()){
            if(ofRandom(0.0, 1.0) > 0.5){ 
               toggleState();
            }{
                invertColor();
            }
        }
    }
    if(m_autoFlip){
        // flip one of the textures at n intervals
        const float flipTime = 0.2;
        m_horizontalFlip = static_cast<size_t>( ofNoise(ofGetElapsedTimef() * flipTime ) * 2 );

        const float invertTime = 0.1;
        m_invertColor = static_cast<size_t>( ofNoise(ofGetElapsedTimef() * invertTime ) * 2 );
    }
}


void CamGlitch::draw() {

    m_shader.begin();
    m_shader.setUniform1i("h", h);
    m_shader.setUniform1i("w", w);
    m_shader.setUniform1i("horizontalFlip", m_horizontalFlip);
    m_shader.setUniform1i("invertColor", m_invertColor);
    m_shader.setUniform1f("dimmer", dimmer);
    m_shader.setUniform1f("red", m_red);
    m_shader.setUniform1f("green", m_green);
    m_shader.setUniform1f("blue", m_blue);
    
    m_shader.setUniform1f("rotation", m_rotation);

    m_shader.setUniformTexture("tex0", m_image.getTexture(), 1);
    m_shader.setUniformTexture("tex1", m_image.getTexture(), 2);

    m_image.draw(0, 0);

    m_shader.end();

    if(showGui){
        ofDrawBitmapString(ofToString(m_red), 20, 20);
        ofDrawBitmapString(ofToString(m_green), 20, 40);
        ofDrawBitmapString(ofToString(m_blue), 20, 60);
    }
}

void CamGlitch::keyPressed(int key) {
    switch (key) {
        case 't': toggleState(); break;
        case 'i': invertColor(); break;
        default: {}
    }
}

void CamGlitch::play() {
}

void CamGlitch::pause() {
}

void CamGlitch::toggleState(){
    m_horizontalFlip = 1 - m_horizontalFlip;
}

void CamGlitch::invertColor(){
    m_invertColor = 1 - m_invertColor;
}

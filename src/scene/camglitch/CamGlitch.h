#pragma once

#include "cameraDeltaTrigger.h"
#include "ofMain.h"
#include "Configuration.h"
#include "Scene.h"

class CamGlitch: public Scene {
public:

    void setup() override;
    void update() override;
    void draw() override;
    void keyPressed(int key) override;
    void play() override;
    void pause() override;
    void toggleState() override;
    void invertColor() override;

private:
    const float m_defaultRotation = 1.0;

    float m_rotation = m_defaultRotation;

    ofShader m_shader;
    ofImage m_fireplaceimg;
    ofPixels m_pixels;
    ofImage m_image;
    const float m_red = 0.77;
    const float m_green = 1.0;
    const float m_blue = 0.33;
    size_t m_horizontalFlip = 0;
    size_t m_invertColor = 0;
    bool m_autoFlip = false;
    
    CameraDeltaTrigger m_cameraDeltaTrigger;
};


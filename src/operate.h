#pragma once

// In a production system test cases can be disabled.
//#define WITH_testcases
#undef WITH_testcases

// ofxCv
// Download from: https://github.com/kylemcdonald/ofxCv
//
// to enable ofxCv:
//   add an ofxCv line to addons.make
//   define WITH_Cv
// #define WITH_Cv
#undef WITH_Cv

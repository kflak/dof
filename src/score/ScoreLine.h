#pragma once
#include "ofMain.h"

enum {
    ScorelineScrollHorizontal,
    ScorelineScrollVertical,
    ScoreLinePerlinHorizontal,
    ScoreLinePerlinVertical,
    ScoreLineStatic
};

class ScoreLine {
public:
    ScoreLine();
    ~ScoreLine();    

    void setup(ofVec2f startPos, int width, int height);
    void update();
    void draw();

    auto inline getX() const -> float{ return m_pos.x;};
    auto inline getY() const -> float{ return m_pos.y;};

    float speed = 0.1;
    float offset = 0.0;
    int hue;
    int sat;
    int val;
    int alpha;

    size_t mode = ScoreLinePerlinHorizontal;
    float depth = 1000.0;



private:
    ofVec3f m_pos;
    int m_height;
    int m_width;

    ofColor m_color;
};

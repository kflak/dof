#include "ScoreLine.h"
#include "globalvars.h"

ScoreLine::ScoreLine() = default;

ScoreLine::~ScoreLine() = default;

void ScoreLine::setup(ofVec2f startPos, int width = centerW, int height = 10){
// void ScoreLine::setup(float startX=100, float startY=100, int width = centerW, int height = 10){

    m_pos = startPos;
    m_width = width;
    m_height = height;
    hue = 255;
    sat = 255;
    val = 255;
    alpha = 255;
}


void ScoreLine::update(){
    switch(mode) {
        case ScorelineScrollHorizontal:
            m_pos.x += speed;
            // ofLog() << "ScorelineScrollHorizontal " << m_pos.x;
            break;
        case ScorelineScrollVertical: 
            m_pos.y += speed;
            // ofLog() << "ScoreLineScrollVertical " << m_pos.y;
            break;
        case ScoreLinePerlinHorizontal: 
            m_pos.x += ofSignedNoise(ofGetElapsedTimef() * speed + offset) * depth;
            m_pos.x = ofClamp(m_pos.x, 0, w);
            break;
        case ScoreLinePerlinVertical :
            m_pos.y += ofSignedNoise(ofGetElapsedTimef() * speed + offset) * depth;
            m_pos.y = ofClamp(m_pos.y, 0, h);
            break;
    }
    
}

void ScoreLine::draw(){
    ofEnableAlphaBlending();
    m_color = ofColor::fromHsb(hue, sat, val, alpha);
    ofSetLineWidth(m_width);
    ofSetColor(m_color);
    ofDrawRectangle(m_pos, m_width, m_height);
    ofDisableAlphaBlending();
}


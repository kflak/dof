#include "globalvars.h"

unsigned int frame = 0;

float dimmer = 1.0;

ofxXmlSettings setting;

bool showGui = false;

const int w = Configuration::windowWidth;
const int h = Configuration::windowHeight;
const int centerW = w / 2;
const int centerH = h / 2;

std::map<string, shared_ptr<ofVideoPlayer>> movie;

int camWidth  = 640;
int camHeight = 480;
ofVideoGrabber webcam;

bool autoMix = false;

float cameraDelta = 0;
bool enableCameraDelta = false;

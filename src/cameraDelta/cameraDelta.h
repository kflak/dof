#pragma once

#include "ofMain.h"

class CameraDelta {
   public:
    void setup();
    void update();

private:
    ofPixels m_prevImage;
    size_t m_numChannels;
};

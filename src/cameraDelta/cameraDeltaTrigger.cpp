#include "cameraDeltaTrigger.h"

void CameraDeltaTrigger::setup(float threshold = 0.1, float speedlim = 1){
    m_threshold = threshold;
    m_speedlim = speedlim;
    m_time0 = ofGetElapsedTimef();
}

bool CameraDeltaTrigger::getTrigger(){

    const float currentTime = ofGetElapsedTimef();
    const float time = currentTime - m_time0;

    if((cameraDelta > m_threshold) && (time > m_speedlim)){
        m_time0 = currentTime;
        return true;
    }{
        return false;
    }

}

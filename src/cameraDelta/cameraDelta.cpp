#include "cameraDelta.h"
#include "globalvars.h"

void CameraDelta::setup() {
    m_prevImage = webcam.getPixels();
    m_numChannels = m_prevImage.getNumChannels();
}

void CameraDelta::update() {
    if(webcam.isFrameNew()) {
        ofPixels currentImage = webcam.getPixels();

        size_t imgSize = currentImage.size();
        size_t delta = 0;
        
        for (size_t i = 0; i < imgSize; i+= m_numChannels) {
            delta += abs(currentImage[i] - m_prevImage[i]);
        }

        cameraDelta = delta / imgSize / 255.0;
        m_prevImage = currentImage;
    }
}

#pragma once
#include "globalvars.h"
#include "ofMain.h"

class CameraDeltaTrigger {
   public:
    void setup(float threshold, float speedlim);
    bool getTrigger();

   private:
    float m_threshold;
    float m_speedlim;
    float m_time0;
};

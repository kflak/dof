---
geometry: a4paper
title: Perlin's Physarum - for Piano and Clarinet
author: Kenneth Flak
titlepage: false
disable-header-and-footer: false
date: 2023-02-27
urlcolor: magenta
---

# Perlin's Physarum

The piece is based on a simulation of the growth patterns of the
[physarum
polycephalum](https://en.wikipedia.org/wiki/Physarum_polycephalum), a
slime mold with the capacity to solve
complex computational problems, representing a promising possibility for
bio-computing. Different parameters of the simulation are controlled
by [Perlin noise](https://en.wikipedia.org/wiki/Perlin_noise) with different scalings and tempi.

## Performance Instructions

### Pitch

The screen is divided into a grid of 8 x 4 sections. Each vertical line
represents an octave on the keyboard, with the left being the lowest
octave, the right being the highest. Each horizontal line represents an
octave of the clarinet.

The left-most horizontal rectangle represents the left hand of the
piano. The right-most horizontal rectangle represents the right hand.
Both rectangles span a single octave each. The vertical rectangle
represents the clarinet, and spans an octave in the vertical dimension.

Whenever a part of the physarum enters a rectangle, this is interpreted
as pitch information.

If more than one figure fills the rectangle, this can be interpreted as
a chord (for the piano), as an arpeggio (for piano and clarinet), or as
a prompt for multiphonics (clarinet). The pianist should not play more
than three consecutive chords or arpeggios.

Occasionally there will be a single "creature" climbing around in the
rectangle. This should be traced with single notes.

### Dynamics

The thickness of the physarum is interpreted as dynamics, from barely
audible to the loudest possible sound when the entire rectangle is
almost filled. Additionally, the dynamics is determined by the opacity
and color temperature of the rectangle: The fainter the rectangle, the
quieter the instrument. This also corresponds to a more reddish tone.
Nothing should be played when the rectangle is not visible.

### Corner cases

If the right-hand rectangle crosses the clarinet-rectangle, the
clarinet player uses the piano as a resonance box.

If the rectangle contains no traces, the clarinet player blows air
through the instrument; the piano player taps on the keys with their
fingernails.

If the rectangle appears and disappears too fast to clearly discern
pitch information, speak the word "exit" in a detached manner.

If the rectangle is completely filled, perform one of these actions:

#### Piano

- Stretch out both arms in random directions
- Attempt to lift the piano
- Nod head up and and down
- Shake head from side to side

#### Clarinet

- Lift the instrument above the head with both hands
- Jump or jog repeatedly in place
- Point the clarinet at a random location
- Create circles with the head

## Rhythm

The general principle for the rhythm is to be as slow as possible, yet as
fast as necessary to convey the pitch material.

## Source Code

The source code for the piece depends on
[openFrameworks](https://openframeworks.cc), and can be found here:
[https://gitlab.com/kflak/dof](https://gitlab.com/kflak/dof/-/blob/main/
src/scene/physarum/BoxPhysarum.h).

The physarum simulation is based on Samuel Cho's [implementation](https://github.com/chosamuel/openframeworks-physarum) of 
slime mold growth patterns, and further developed by [Thomas Jourdan](https://metagrowing.org).

The code is released under the [GNU General Public Licence](https://www.gnu.org/licenses/).
